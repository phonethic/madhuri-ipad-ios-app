//
//  MadhuriMovies.h
//  MadhuriDixit
//
//  Created by Rishi on 15/10/12.
//  Copyright (c) 2012 Phonethics. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MadhuriMovies : NSObject {
    
}

@property (nonatomic, copy) NSString *movieYear;
@property (nonatomic, copy) NSString *moviename;
@property (nonatomic, copy) NSString *movieImages;
@property (nonatomic, copy) NSString *movieAwards;
@property (nonatomic, copy) NSString *movieDirector;
@property (nonatomic, copy) NSString *movieProducer;
@property (nonatomic, copy) NSString *movieActedAs;
@property (nonatomic, copy) NSString *movieAnecdotes;
@property (nonatomic, copy) NSString *movieSynopsis;

@end
