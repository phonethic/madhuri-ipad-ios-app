//
//  MDMainViewController.m
//  MadhuriDixitHD
//
//  Created by Rishi on 17/01/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import "MDMainViewController.h"
#import "MDAppDelegate.h"
#import "ImageViewController.h"
#import "MDTitleViewController.h"
#import "MDLatestViewController.h"
#import "MDMoviesViewController.h"
#import "MDArtViewController.h"
#import "MDDanceViewController.h"
#import "MDFashionViewController.h"
#import "MDHealthBeautyViewController.h"
#import "MDWebViewController.h"
#import "MDGalleryViewController.h"
#import "SBJson.h"


NSUInteger kHomeNumberOfPages;
NSUInteger kHomeNumberOfTitles;

//#define MOVIES_LINK @"http://madhuridixit-nene.com/uploads/gallery.xml"
//#define MOVIES_LINK @"http://madhuridixit-nene.com/wp/?json=get_page&dev=1&id=301"

@interface MDMainViewController ()
{
    CGPoint lastOffset1;
    NSTimeInterval lastOffsetCapture;
    CGFloat scrollSpeed;
    
    BOOL isScrollingFast;
    
    BOOL isAnimationStarted;
}
@property (assign) NSUInteger page;
@property (assign) NSUInteger page1;
- (void)useNotificationWithString:(NSNotification*)notification;
@end

@implementation MDMainViewController
@synthesize menuView;
@synthesize flipViewBtn;
@synthesize facebookBtn;
@synthesize twitterBtn;
@synthesize menutextImageView;
@synthesize taptoenterView;
@synthesize scrollView;
@synthesize titleScrollView;
@synthesize slidingImageView;
@synthesize backImageView;
@synthesize pageControl;
@synthesize pageControl1;
@synthesize viewControllers;
@synthesize page = _page;
@synthesize page1 = _page1;
@synthesize titleviewControllers;
@synthesize photos = _photos;

- (void)viewWillAppear:(BOOL)animated {
	[super viewWillAppear:animated];
    //   DebugLog(@"viewWillAppear %d and %d",_page,pageControl.currentPage);
	for (NSUInteger i =0; i < [viewControllers count]; i++) {
		[self loadScrollViewWithPage:i type:1];
	}
    
	[self.pageControl setNumberOfPages:kHomeNumberOfPages];
    
	UIViewController *viewController = [viewControllers objectAtIndex:self.pageControl.currentPage];
	if (viewController.view.superview != nil) {
		[viewController viewWillAppear:animated];
	}
    
	for (NSUInteger i =0; i < [titleviewControllers count]; i++) {
		[self loadScrollViewWithPage:i type:2];
	}
    
	[self.pageControl1 setNumberOfPages:kHomeNumberOfTitles];
    
	UIViewController *viewController1 = [titleviewControllers objectAtIndex:self.pageControl1.currentPage];
	if (viewController1.view.superview != nil) {
		[viewController1 viewWillAppear:animated];
	}
    
    [self.navigationController setNavigationBarHidden:YES animated:NO];
	//self.scrollView.contentSize = CGSizeMake(scrollView.frame.size.width * kHomeNumberOfPages, scrollView.frame.size.height);
}


- (void)viewDidAppear:(BOOL)animated {
	[super viewDidAppear:animated];
    //DebugLog(@"viewDidAppear %d and %d",_page,pageControl.currentPage);
	UIViewController *viewController = [viewControllers objectAtIndex:self.pageControl.currentPage];
	if (viewController.view.superview != nil) {
		[viewController viewDidAppear:animated];
	}
    
    UIViewController *viewController1 = [titleviewControllers objectAtIndex:self.pageControl1.currentPage];
	if (viewController1.view.superview != nil) {
		[viewController1 viewDidAppear:animated];
	}
}

- (BOOL)automaticallyForwardAppearanceAndRotationMethodsToChildViewControllers {
	return NO;
}


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    pageChanged = 0;
    homeImages=[[NSArray alloc]initWithObjects:
                @"title_all.png",@"sandvich0a.png",@"sandvich0b.png",
                @"transp.png",@"sandvich1a.png",@"sandvich1b.png",
                @"transp.png",@"sandvich2a.png",@"sandvich2b.png",
                @"transp.png",@"sandvich3a.png",@"sandvich3b.png",
                @"transp.png",@"sandvich4a.png",@"sandvich4b.png",
                @"transp.png",@"sandvich5a.png",@"sandvich5b.png",
                @"transp.png",@"sandvich6a.png",@"sandvich6b.png",
                @"transp.png",nil];
    
    kHomeNumberOfPages=[homeImages count];
    
    NSMutableArray *controllers = [[NSMutableArray alloc] init];
    for (unsigned i = 0; i < kHomeNumberOfPages; i++)
    {
		[controllers addObject:[NSNull null]];
    }
    self.viewControllers = controllers;
    
    // a page is the width of the scroll view
    scrollView.clipsToBounds = YES;
	scrollView.scrollEnabled = NO;
    scrollView.pagingEnabled = YES;
    scrollView.contentSize = CGSizeMake(scrollView.frame.size.width * kHomeNumberOfPages, scrollView.frame.size.height);
    scrollView.showsHorizontalScrollIndicator = NO;
    scrollView.showsVerticalScrollIndicator = NO;
    scrollView.scrollsToTop = YES;
    scrollView.delegate = self;
    scrollView.bounces = NO;
    scrollView.directionalLockEnabled = YES;
    
    pageControl.autoresizingMask = UIViewAutoresizingFlexibleWidth ;
    //[self.view bringSubviewToFront:pageControl];
    
    [self loadScrollViewWithPage:0 type:1];
    [self loadScrollViewWithPage:1 type:1];
    
    
    //------------------------------------------------------------------------------------------------------------------------------//
    
    
    titleImages=[[NSArray alloc]initWithObjects:
                 @"slider1_transp.png",@"slider1_transp.png",
                 @"sliderbutton1",@"slider1_transp.png",
                 @"sliderbutton2",@"slider1_transp.png",
                 @"sliderbutton3",@"slider1_transp.png",
                 @"sliderbutton4",@"slider1_transp.png",
                 @"sliderbutton5",@"slider1_transp.png",
                 @"sliderbutton6",@"slider1_transp.png",
                 @"sliderbutton7",nil];
    kHomeNumberOfTitles = [titleImages count];
    
    NSMutableArray *controllers1 = [[NSMutableArray alloc] init];
    for (unsigned i = 0; i < kHomeNumberOfTitles; i++)
    {
		[controllers1 addObject:[NSNull null]];
    }
    self.titleviewControllers = controllers1;
    
    // a page is the width of the scroll view
    titleScrollView.clipsToBounds = YES;
	titleScrollView.scrollEnabled = YES;
    titleScrollView.pagingEnabled = YES;
    titleScrollView.contentSize = CGSizeMake(titleScrollView.frame.size.width * kHomeNumberOfTitles, titleScrollView.frame.size.height );
    titleScrollView.showsHorizontalScrollIndicator = NO;
    titleScrollView.showsVerticalScrollIndicator = NO;
    titleScrollView.scrollsToTop = YES;
    titleScrollView.delegate = self;
    titleScrollView.bounces = YES;
    titleScrollView.directionalLockEnabled = YES;
    
    pageControl1.autoresizingMask = UIViewAutoresizingFlexibleWidth ;
    //[self.view bringSubviewToFront:pageControl];
    
    scrollSpeed=0;
    [self loadScrollViewWithPage:0 type:2];
    [self loadScrollViewWithPage:1 type:2];
    
    UITapGestureRecognizer *gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(enterapp:)];
    gestureRecognizer.delegate = self;
    [scrollView addGestureRecognizer:gestureRecognizer];
    
    //NSString *notificationName = @"MadhuriTitleNotification";
    
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(useNotificationWithString:)
	 name:titlenotificationName
	 object:nil];
    
    //[self.view bringSubviewToFront:self.scrollView];
    [self.view bringSubviewToFront:self.flipViewBtn];
    [self.menuView setHidden:TRUE];
    [self.taptoenterView setHidden:TRUE];

}

/* previous gallery code
-(void)projectListAsynchronousCall
{
	
    DebugLog(@"galleryArray = %@",galleryPhotos);
    
    btn_pressed = 1;
    
	NSURLRequest *urlRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:MOVIES_LINK] cachePolicy:YES timeoutInterval:10.0];
    
	// Note: An NSOperation creates an autorelease pool, but doesn't schedule a run loop
	// Create the connection and schedule it on a run loop under our namespaced run mode
	[[NSURLConnection alloc] initWithRequest:urlRequest delegate:self startImmediately:YES];
	
}

#pragma mark NSURLConnection Delegate Methods

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
	
	if ([response isKindOfClass:[NSHTTPURLResponse class]])
	{
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
        
		status = [httpResponse statusCode];
	}
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
	
	if(responseAsyncData==nil)
	{
		responseAsyncData = [[NSMutableData alloc] initWithLength:0];
	}
    
	[responseAsyncData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
	[self connectionDidFinishLoading:nil];
}


- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
	
	NSString *xmlDataFromChannelSchemes;
    
	if(responseAsyncData)
	{
//		NSString *result = [[NSString alloc] initWithData:responseAsyncData encoding:NSASCIIStringEncoding];
//		//DebugLog(@"\n result:%@\n\n", result);
//        [MDHDAppDelegate writeToTextFile:result name:@"gallery"];
//		xmlDataFromChannelSchemes = [[NSString alloc] initWithString:result];
//		NSData *xmlDataInNSData = [xmlDataFromChannelSchemes dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
//		xmlParser = [[NSXMLParser alloc] initWithData:xmlDataInNSData];
//		[xmlParser setDelegate:self];
//		[xmlParser parse];
        
        NSString *responseString = [[NSString alloc] initWithData:responseAsyncData encoding:NSUTF8StringEncoding];
       // DebugLog(@"\n responseString:%@\n\n", responseString);
        [MDHDAppDelegate writeToTextFile:responseString name:@"gallery"];
        [self parseData:responseString];
        responseString = nil;
        self->responseAsyncData = nil;
        
	}
    else
    {
        [self parseFromFile];
    }
    
}

-(void) parseFromFile
{
//    NSString *xmlDataFromChannelSchemes;
//    NSString *data;
//    data = [MDHDAppDelegate getTextFromFile:@"gallery"];
//    //DebugLog(@"\n data:%@\n\n", data);
//    if(![data isEqualToString:@""])
//    {
//        xmlDataFromChannelSchemes = [[NSString alloc] initWithString:data];
//        NSData *xmlDataInNSData = [xmlDataFromChannelSchemes dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
//        xmlParser = [[NSXMLParser alloc] initWithData:xmlDataInNSData];
//        [xmlParser setDelegate:self];
//        [xmlParser parse];
//    }
//    else
//    {
//        //DebugLog(@"\n null data\n");
//        btn_pressed = 0;
//        UIAlertView *alertView = [[UIAlertView alloc]
//                                  initWithTitle:@"No Network Connection"
//                                  message:@"Please check your internet connection and try again."
//                                  delegate:self
//                                  cancelButtonTitle:@"OK"
//                                  otherButtonTitles:nil];
//        [alertView show];
//    }
    DebugLog(@"parsing from file ");
    NSString *responseString = [[NSString alloc] initWithString:[MDHDAppDelegate getTextFromFile:@"gallery"]];
    if(![responseString isEqualToString:@""])
    {
        [self parseData:responseString];
    }
    else
    {
        btn_pressed = 0;
        UIAlertView *errorView = [[UIAlertView alloc]
                                  initWithTitle:@"No Network Connection"
                                  message:@"Please check your internet connection and try again."
                                  delegate:self
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil];
        [errorView show];
    }
    responseString = nil;
    self->responseAsyncData = nil;
}

-(void)parseData:(NSString *)dataString
{
    //Get json value
    NSDictionary* mainDict = (NSDictionary*)[dataString JSONValue];

    NSString *reqStatus = [mainDict objectForKey:@"status"];
    DebugLog(@"reqStatus %@",reqStatus);
    if ([reqStatus isEqualToString:@"ok"])
    {
        //Get address dictionary
        NSDictionary* pageDict = [mainDict objectForKey:@"page"];
        NSArray *attachmentArray    =   [pageDict objectForKey:@"attachments"];
       // DebugLog(@"attachmentArray : %@",attachmentArray);
        
        if (galleryPhotos == nil) {
            galleryPhotos = [[NSMutableArray alloc] init];
        }
        if (attachmentArray.count > galleryPhotos.count)
        {
            for(NSDictionary *attachmentDict in attachmentArray)
            {
                DebugLog(@"..");
                //Get phone number array
                //NSDictionary* imagesDict    =   [attachmentDict objectForKey:@"images"];
                //NSDictionary* fullDict      =   [imagesDict objectForKey:@"full"];
                NSString * fullImageUrl     =   [attachmentDict objectForKey:@"url"];
              //  DebugLog(@"fullImageUrl %@",fullImageUrl);

                [self isMember:fullImageUrl];
                    [galleryPhotos addObject:fullImageUrl];
            }
            DebugLog(@"Done parsing.. array %@",galleryPhotos);
        }
        [self pushGalleryController];
    }
}
-(BOOL)isMember:(NSString *)currentUrl
{
    for (NSString *oldUrl in galleryPhotos)
    {
        if([oldUrl isEqualToString:currentUrl])
            return YES;
    }
    return NO;
}
#pragma mark xmlParser methods
// Called when the parser runs into an open tag (<tag>) 
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qualifiedName 	attributes:(NSDictionary *)attributeDict
{
	if([elementName isEqualToString:@"gallery"])
	{
		galleryPhotos = [[NSMutableArray alloc] init];
	} else if([elementName isEqualToString:@"photo"]) {
        //DebugLog(@"%@", [attributeDict objectForKey:@"link"]);
        [galleryPhotos addObject:[attributeDict objectForKey:@"link"]];
    }
}

-(void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string {
    
}

// Called when the parser runs into a close tag (</tag>). 
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName
{
    if ([elementName isEqualToString:@"gallery"]) {
        //DebugLog(@"%d",[galleryPhotos count]);
        [self pushGalleryController];
    }
}

- (void)parser:(NSXMLParser *)parser parseErrorOccurred:(NSError *)parseError
{
    //DebugLog(@"Error: %@", [parseError localizedDescription]);
}

- (void)parser:(NSXMLParser *)parser validationErrorOccurred:(NSError *)validationError
{
    //DebugLog(@"Error: %@", [validationError localizedDescription]);
}
*/
- (void)useNotificationWithString:(NSNotification *)notification //use notification method and logic
{
    NSDictionary *dictionary = [notification userInfo];
    NSString *stringValue = [dictionary valueForKey:titlenotificationKey];
    [self openController:[stringValue integerValue]];
    
}

-(void)viewControllerFlipanimation:(UIViewController *)viewController{
    [UIView  transitionWithView:self.navigationController.view duration:1.0  options:UIViewAnimationOptionTransitionFlipFromLeft
                     animations:^(void) {
                         BOOL oldState = [UIView areAnimationsEnabled];
                         [UIView setAnimationsEnabled:NO];
                         [self.navigationController pushViewController:viewController animated:NO];
                         [UIView setAnimationsEnabled:oldState];
                     }
                     completion:nil];
}

-(void)openController : (int) lpage
{
    //[MDHDAppDelegate playAudioOnButtonPressed];
    switch (lpage) {
        case 2:
        {
            //[self logTimePerSection:@"Latest" eventName:@"Latest_Session"];
            MDLatestViewController *viewController = [[MDLatestViewController alloc] initWithNibName:@"MDLatestViewController" bundle:nil];
            viewController.title = @"Latest";
            [self viewControllerFlipanimation:viewController];
            
        }
            break;
            
        case 4:
        {
            //[self logTimePerSection:@"Dance" eventName:@"Dance_Session"];
            MDDanceViewController *viewController = [[MDDanceViewController alloc] initWithNibName:@"MDDanceViewController" bundle:nil];
            viewController.title = @"Dance";
            [self viewControllerFlipanimation:viewController];
            
        }
            break;
            
        case 6:
        {
            //[self logTimePerSection:@"Movies" eventName:@"Movies_Session"];
            MDMoviesViewController *viewController = [[MDMoviesViewController alloc] initWithNibName:@"MDMoviesViewController" bundle:nil];
            viewController.title = @"Movies";
            [self viewControllerFlipanimation:viewController];
        }
            break;
            
        case 8:
        {
            //[self logTimePerSection:@"Fashion" eventName:@"Fashion_Session"];
            MDFashionViewController *viewController = [[MDFashionViewController alloc] initWithNibName:@"MDFashionViewController" bundle:nil];
            viewController.title = @"Fashion";
            [self viewControllerFlipanimation:viewController];
            
        }
            break;
            
        case 10:
        {
            //[self logTimePerSection:@"Beauty" eventName:@"Beauty_Session"];
            MDHealthBeautyViewController *viewController = [[MDHealthBeautyViewController alloc] initWithNibName:@"MDHealthBeautyViewController" bundle:nil];
            viewController.title = @"Health & Beauty";
            [self viewControllerFlipanimation:viewController];
        }
            break;
            
        case 12:
        {
            //[self logTimePerSection:@"Art" eventName:@"Art_Session"];
            MDArtViewController *viewController = [[MDArtViewController alloc] initWithNibName:@"MDArtViewController" bundle:nil];
            viewController.title = @"Art";
            [self viewControllerFlipanimation:viewController];
        }
            break;
            
        case 14:
        {
//            if(btn_pressed==0)
//                [self projectListAsynchronousCall];
            MDGalleryViewController *viewController = [[MDGalleryViewController alloc] initWithNibName:@"MDGalleryViewController" bundle:nil];
           // viewController.title = @"Gallery";
            [self viewControllerFlipanimation:viewController];
        }
            break;
            
        default:
            break;
            
            
    }
    //[self.titleScrollView setFrame:CGRectMake(0, 377, self.titleScrollView.frame.size.width, self.titleScrollView.frame.size.height)];
    
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    UITouch *touch=[touches anyObject];
    CGPoint point=[touch locationInView:self.scrollView];
    if ((point.x>=260 && point.x <= 320) && (point.y>=332 && point.y<=364))
    {
        // DebugLog(@"began point %f %f",point.x,point.y);
        if (_page==0)
        {
            ImageViewController *viewController = [viewControllers objectAtIndex:0];
            viewController.imgView.image=[UIImage imageNamed:@"title_all_selected.png"];
            [self changeMainView:1 duration:2.0];
            [UIView beginAnimations:@"titleScrollView" context:nil];
            [UIView setAnimationDuration:2.0];
            [UIView setAnimationDelegate:self];
            [UIView setAnimationTransition:UIViewAnimationOptionTransitionNone forView:self.titleScrollView cache:YES];
            [self.titleScrollView setContentOffset:CGPointMake(self.titleScrollView.contentOffset.x+(self.titleScrollView.frame.size.width*2),0) animated:NO];
            [UIView commitAnimations];
            _page1 = _page1 + 1;
            self.pageControl1.currentPage = _page1 ;
            lastTitleOffset=self.titleScrollView.contentOffset.x;
            self.titleScrollView.scrollEnabled=YES;
            scrollView.scrollEnabled = NO;
        }
    }
}

-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event{
    if (_page==3)
    {
        ImageViewController *viewController = [viewControllers objectAtIndex:0];
        viewController.imgView.image=[UIImage imageNamed:@"title_all.png"];
    }
}

-(void) enterapp:(id) sender
{
    // Do whatever such as hiding the keyboard
    if(_page1==2||_page1==4||_page1==6||_page1==8||_page1==10||_page1==12||_page1==14)
        [self openController:_page1];
    //DebugLog(@"heyy  :) %d",_page1);
}


- (void)viewWillDisappear:(BOOL)animated {
    // DebugLog(@"viewWillDisappear %d and %d",_page,pageControl.currentPage);
	UIViewController *viewController = [viewControllers objectAtIndex:self.pageControl.currentPage];
	if (viewController.view.superview != nil) {
		[viewController viewWillDisappear:animated];
	}
    [self.menuView setHidden:TRUE];
    [flipViewBtn setImage:[UIImage imageNamed:@"menu_icon_off"] forState:UIControlStateNormal];
    [flipViewBtn setImage:[UIImage imageNamed:@"menu_icon_on"] forState:UIControlStateHighlighted];
	[super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated {
    //  DebugLog(@"viewDidDisappear %d and %d",_page,pageControl.currentPage);
    
	UIViewController *viewController = [viewControllers objectAtIndex:self.pageControl.currentPage];
	if (viewController.view.superview != nil) {
		[viewController viewDidDisappear:animated];
	}
	[super viewDidDisappear:animated];
}


- (void)viewDidUnload
{
    [self setScrollView:nil];
    [self setTitleScrollView:nil];
    [self setSlidingImageView:nil];
    [self setBackImageView:nil];
    [self setPageControl:nil];
    [self setPageControl1:nil];
    [self setMenutextImageView:nil];
    [self setTaptoenterView:nil];
    [self setFlipViewBtn:nil];
    [self setFacebookBtn:nil];
    [self setTwitterBtn:nil];
    [self setMenuView:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

-(void)menutransitionDidStop:(NSString *)animationID finished:(NSNumber *)finished context:(void *)context {
    if([animationID isEqualToString:@"SHOW_MENU"]) {
        [self.menuView setHidden:FALSE];
    } else if([animationID isEqualToString:@"HIDE_MENU"]) {
        [self.menuView setHidden:TRUE];
    }
}

- (void)loadScrollViewWithPage:(int)page type:(int) scrollviewType {
    
    if(scrollviewType == 1) {
        if (page < 0) return;
        if (page >= kHomeNumberOfPages) return;
        
        // replace the placeholder if necessary
        UIViewController *controller = [viewControllers objectAtIndex:page];
        if ((NSNull *)controller == [NSNull null])
        {
            controller = [[ImageViewController alloc]initWithPageNumber:page imageName:[homeImages objectAtIndex:page] height:845];
            
            [viewControllers replaceObjectAtIndex:page withObject:controller];
        }
        
        // add the controller's view to the scroll view
        if (controller.view.superview == nil) {
            CGRect frame = self.scrollView.frame;
            frame.origin.x = frame.size.width * page;
            frame.origin.y = 0;
            controller.view.frame = frame;
            [self.scrollView addSubview:controller.view];
        }
    } else {
        DebugLog(@"kHomeNumberOfTitles %d page = %d",kHomeNumberOfTitles,page);
        if (page < 0) return;
        if (page >= kHomeNumberOfTitles) return;
        
        // replace the placeholder if necessary
        UIViewController *controller1 = [titleviewControllers objectAtIndex:page];
        if ((NSNull *)controller1 == [NSNull null])
        {
            NSString *imageName=[titleImages objectAtIndex:page];
            if ([imageName isEqualToString:@"slider1_transp.png"])
            {
                controller1 = [[MDTitleViewController alloc]initWithPageNumber:page unpressedImageName:imageName pressedImageName:imageName];
            }
            else{
                controller1 = [[MDTitleViewController alloc]initWithPageNumber:page unpressedImageName:imageName pressedImageName:[NSString stringWithFormat:@"%@_selected",imageName]];
            }
            
            [titleviewControllers replaceObjectAtIndex:page withObject:controller1];
        }
        
        // add the controller's view to the scroll view
        if (controller1.view.superview == nil) {
            CGRect frame = self.titleScrollView.frame;
            frame.origin.x = frame.size.width * page;
            frame.origin.y = 0;
            controller1.view.frame = frame;
            [self.titleScrollView addSubview:controller1.view];
        }
    }
    
}

- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView {
    
    DebugLog(@"scrollViewDidEndScrollingAnimation ");
	UIViewController *oldViewController = [viewControllers objectAtIndex:_page];
	UIViewController *newViewController = [viewControllers objectAtIndex:self.pageControl.currentPage];
	[oldViewController viewDidDisappear:YES];
	[newViewController viewDidAppear:YES];
    
	_page = self.pageControl.currentPage;
    _page1 = self.pageControl1.currentPage;
}

- (void)scrollViewDidScroll:(UIScrollView *)sender
{
    // We don't want a "feedback loop" between the UIPageControl and the scroll delegate in
    // which a scroll event generated from the user hitting the page control triggers updates from
    // the delegate method. We use a boolean to disable the delegate logic when the page control is used.
    [self.taptoenterView setHidden:TRUE];
    CGPoint currentOffset1 = self.titleScrollView.contentOffset;  //if
    if(currentOffset1.x < 0)
    {
        return;
    }
    if (_page == 3)
    {
        [self.menutextImageView setHidden:TRUE];
    }
    CGPoint currentOffset = self.titleScrollView.contentOffset;
    NSTimeInterval currentTime = [NSDate timeIntervalSinceReferenceDate];
    
    NSTimeInterval timeDiff = currentTime - lastOffsetCapture;
    if(timeDiff > 0.1) {
        CGFloat distance = currentOffset.x - lastOffset1.x;
        //The multiply by 10, / 1000 isn't really necessary.......
        CGFloat scrollSpeedNotAbs = (distance * 10) / 1000; //in pixels per millisecond
        
        
        //DebugLog(@"************>>scrollSpeed %f",scrollSpeed);
        //   if (scrollSpeed == 0)
        {
            scrollSpeed = fabsf(scrollSpeedNotAbs);
            if (scrollSpeed > 2.4) {
                isScrollingFast = YES;
                //DebugLog(@"Fast");
            } else {
                isScrollingFast = NO;
                //DebugLog(@"Slow");
            }
        }
        lastOffset1 = currentOffset;
        lastOffsetCapture = currentTime;
    }
    if (isScrollingFast && isAnimationStarted) //|| (!isScrollingFast && isAnimationStarted))
    {
        return;
    }
    
    int tempadd=0;
    if (pageControlUsed)
    {
        // do nothing - the scroll was initiated from the page control, not the user dragging
        return;
    } else {
        if (!FLAG)
        {// if user is starts dragging but not scrolled equal or greater than 50% of title scroll
            
            tempadd = ((int)(self.titleScrollView.contentOffset.x)%768); //to adjust the scrollview size
            //from left or right depends on the titleScrollview
            
            //   DebugLog(@"tempadd %d",tempadd);
            
            if (tempadd!=0)
            {
                if (lastTitleOffset < self.titleScrollView.contentOffset.x) //if scrolls titleScrollview forward side
                {
                    self.scrollView.contentOffset = CGPointMake((self.titleScrollView.contentOffset.x+self.view.frame.size.width*_page/3)+tempadd , 0);
                }
                else //if scrolls titleScrollview back side
                {
                    tempadd=self.view.frame.size.width-tempadd;
                    self.scrollView.contentOffset = CGPointMake((self.titleScrollView.contentOffset.x+self.view.frame.size.width*_page/3)-tempadd , 0);
                }
            }
            // self.scrollView.contentOffset = CGPointMake((self.titleScrollView.contentOffset.x+320*_page/3), 0);
        }
    }
}

-(void)fadeOut:(UIView*)viewToDissolve withDuration:(NSTimeInterval)duration
{
    [UIView beginAnimations: @"Fade Out" context:nil];
    // wait for time before begin
    [UIView setAnimationDelay:0];
    // druation of animation
    [UIView setAnimationDuration:duration];
    viewToDissolve.alpha = 0.0;
    [UIView commitAnimations];
}

-(void)fadeIn:(UIView*)viewToFadeIn withDuration:(NSTimeInterval)duration
{
    //DebugLog(@"fade in PC=%d PC1=%d title page=%d mian=%d",pageControl.currentPage,pageControl1.currentPage,_page1,_page);
    switch (pageControl.currentPage) {
        case 0:
            backImageView.image=[UIImage imageNamed:@"bg_new.png"];
            break;
            
        case 3:
            backImageView.image=[UIImage imageNamed:@"latest_home.jpg"];
            break;
            
        case 6:
            backImageView.image=[UIImage imageNamed:@"dance_home.jpg"];
            break;
            
        case 9:
            backImageView.image=[UIImage imageNamed:@"movies_home.jpg"];
            break;
            
        case 12:
            backImageView.image=[UIImage imageNamed:@"fashion_home.jpg"];
            break;
            
        case 15:
            backImageView.image=[UIImage imageNamed:@"health_home.jpg"];
            break;
            
        case 18:
            backImageView.image=[UIImage imageNamed:@"art_home.jpg"];
            break;
            
        case 21:
            backImageView.image=[UIImage imageNamed:@"gallery_home.jpg"];
            break;
            
        default:
            backImageView.image=[UIImage imageNamed:@"bg_new.png"];
            break;
            
    }
    [UIView beginAnimations: @"Fade In" context:nil];
    // wait for time before begin
    [UIView setAnimationDelay:0];
    // druation of animation
    [UIView setAnimationDuration:duration];
    viewToFadeIn.alpha = 1;
    [UIView commitAnimations];
    
}

// At the begin of scroll dragging, reset the boolean used when scrolls originate from the UIPageControl
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    //DebugLog(@"scrollViewWillBeginDragging %d",_page1);
    [self fadeOut:facebookBtn withDuration:0.2];
    [self fadeOut:twitterBtn withDuration:0.2];
    //[facebookBtn setHidden:TRUE];
    //[twitterBtn setHidden:TRUE];
    [self fadeOut:backImageView withDuration:0.2];
    [self fadeIn:slidingImageView withDuration:0.2];
    pageControlUsed = NO;
    FLAG=FALSE;
    if (_page == 0)
    {
        ImageViewController *viewController = [viewControllers objectAtIndex:0];
        
        int tag = [viewController.imgView tag];
        //DebugLog(@"tag = %d",tag);
        if (tag==1000) {
            viewController.imgView.image=[UIImage imageNamed:@"title_all_new_selected.png"];
            [self.menutextImageView setHidden:TRUE];
            //[viewController.imgView setAccessibilityIdentifier:@"title_all_new.png"] ;
            viewController.imgView.tag = 1000;
        } else {
            viewController.imgView.image=[UIImage imageNamed:@"title_all_selected.png"];
            //[viewController.imgView setAccessibilityIdentifier:@"title_all.png"] ;
            viewController.imgView.tag = 0;
            [self.menutextImageView setHidden:FALSE];
        }
        return;
    }
    
    //DebugLog(@"pre main page %d, title page %d",_page,_page1);
    //DebugLog(@"pre MAinScrollView %f TitleScrollView %f",self.scrollView.contentOffset.x,self.titleScrollView.contentOffset.x);
}

// At the end of scroll animation, reset the boolean used when scrolls originate from the UIPageControl
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    //DebugLog(@"scrollViewDidEndDecelerating ");
    [self fadeIn:facebookBtn withDuration:0.5];
    [self fadeIn:twitterBtn withDuration:0.5];
    //[facebookBtn setHidden:FALSE];
    //[twitterBtn setHidden:FALSE];
    [Flurry logEvent:@"Navigation_Swipe"];
    ImageViewController *viewController;
    pageControlUsed = NO;
    if (_page==0)
    {
        if (self.scrollView.contentOffset.x >= (lastOffset + lastOffset + self.scrollView.frame.size.width)/2)
        {
            [self changeMainView:1 duration:2.0];
            [self changeTitleView:1 duration:2.5];
            [self.view bringSubviewToFront:self.flipViewBtn];
        }
        else
        {
            viewController = [viewControllers objectAtIndex:0];
            
            int tag = [viewController.imgView tag];
            //DebugLog(@"tag = %d",tag);
            if (tag==1000) {
                viewController.imgView.image=[UIImage imageNamed:@"title_all_new.png"];
                //[viewController.imgView setAccessibilityIdentifier:@"title_all_new.png"] ;
                viewController.imgView.tag = 1000;
                [self.menutextImageView setHidden:TRUE];
            } else {
                viewController.imgView.image=[UIImage imageNamed:@"title_all.png"];
                //[viewController.imgView setAccessibilityIdentifier:@"title_all.png"] ;
                viewController.imgView.tag = 0;
                [self.menutextImageView setHidden:FALSE];
            }
        }
    }
    if (_page==3 && self.taptoenterView.tag != 21)
    {
        //DebugLog(@"my tag %d",self.taptoenterView.tag);
        if(pageChanged)
        {
            //DebugLog(@"page changed");
            self.taptoenterView.tag = 21;
            [self.taptoenterView setHidden:TRUE];
        } else {
            self.taptoenterView.tag = 21;
            [self.taptoenterView setHidden:FALSE];
        }
    } else if(_page!=3 && self.taptoenterView.tag == 21) {
        [self.taptoenterView setHidden:TRUE];
        pageChanged = 1;
    } else if(_page!=3 && self.taptoenterView.tag == 20) {
        [self.taptoenterView setHidden:TRUE];
        pageChanged = 0;
    } else if(_page==3 && self.taptoenterView.tag == 21) {
        if(!pageChanged)
            [self.taptoenterView setHidden:FALSE];
    }
    //- - - - - - - - - - - - - - - - - - - -MainScrollView - - - - - - - - - - - - - - - - - - - -
    
    if(scrollView.tag == 2) //if this is titleScrollView
    {
        // [self fadeIn:backImageView withDuration:1.5];
        // [self fadeOut:slidingImageView withDuration:1.5];
        if (lastTitleOffset < self.titleScrollView.contentOffset.x)
        {
            if (self.titleScrollView.contentOffset.x >= (lastTitleOffset + lastTitleOffset + self.titleScrollView.frame.size.width)/2)
            {
                if (isScrollingFast)
                {
                    isAnimationStarted=NO;
                    [self changeMainView:1 duration:0.8];
                    [self changeTitleView:1 duration:0.1];
                }
                else
                {
                    isAnimationStarted=YES;
                    [self changeMainView:1 duration:1.0];
                    [self changeTitleView:1 duration:1.5];
                }
            }
        }
        else
        {
            if(self.titleScrollView.contentOffset.x <= (lastTitleOffset + lastTitleOffset - self.titleScrollView.frame.size.width)/2)
            {
                if (isScrollingFast)
                {
                    isAnimationStarted=NO;
                    [self changeMainView:0 duration:0.8];
                    //  [self.titleScrollView setContentOffset:CGPointMake(self.titleScrollView.contentOffset.x-self.titleScrollView.frame.size.width,0) animated:NO];
                    [self changeTitleView:0 duration:0.1];
                    
                }
                else
                {
                    isAnimationStarted=YES;
                    [self changeMainView:0 duration:1.0];
                    [self changeTitleView:0 duration:1.5];
                    
                }
                
                // DebugLog(@"Page is ?? %d",_page);
                if (_page==0)
                {
                    [self.view bringSubviewToFront:self.flipViewBtn];
                }
            }
        }
    }
    //DebugLog(@"post main page %d, title page %d",_page,_page1);
    //DebugLog(@"post MAinScrollView %f  TitleScrollView %f",self.scrollView.contentOffset.x,self.titleScrollView.contentOffset.x);
    //DebugLog(@"---------------------------------------------------------------");
    
    [self fadeIn:backImageView withDuration:1.5];
    [self fadeOut:slidingImageView withDuration:1.5];
    scrollSpeed=0;
}

- (void)transitionDidStop:(NSString *)animationID finished:(NSNumber *)finished context:(void *)context
{
    //self.titleScrollView.userInteractionEnabled=YES;
    //DebugLog(@"page %d page1 %d\n ",_page,_page1);
    if(_page1==2 || _page1==0)
    {
        ImageViewController *viewController = [viewControllers objectAtIndex:0];
        int tag = [viewController.imgView tag];
        //DebugLog(@"tag = %d",tag);
        if (tag==0) {
            viewController.imgView.image=[UIImage imageNamed:@"title_all_new.png"];
            //[viewController.imgView setAccessibilityIdentifier:@"title_all_new.png"] ;
            viewController.imgView.tag = 1000;
            [self.menutextImageView setHidden:TRUE];
        } else if(tag==1000){
            viewController.imgView.image=[UIImage imageNamed:@"title_all_new.png"];
            viewController.imgView.tag = 1000;
            [self.menutextImageView setHidden:TRUE];
        }
    }
    
    //[self fadeIn:backImageView withDuration:1.5];
    //[self fadeOut:slidingImageView withDuration:1.5];
    
}

-(void) changeMainView:(int)type duration:(double)value
{
    //after this method dont change the scrollview.contentoffset in scrollViewDidScroll method, so making it as TRUE
    FLAG = TRUE;
    double xchangedvalue = 0;
    if (type) //scroll.width 3 times of screen.width multiplied by current page.
    {
        // scrolling forwardside,
        //[self.scrollView setContentOffset:CGPointMake((self.scrollView.frame.size.width*3)*((_page/3)+1),0) animated:NO];
        double haveToScroll=lastOffset+self.scrollView.frame.size.width*3;
        double diff=haveToScroll-self.scrollView.contentOffset.x;
       // [self.scrollView setContentOffset:CGPointMake(self.scrollView.contentOffset.x+diff,0) animated:NO];
        xchangedvalue = self.scrollView.contentOffset.x+diff;
        _page = _page + 3;
    }
    else
    {
        // scrolling backside,
        //[self.scrollView setContentOffset:CGPointMake((self.scrollView.frame.size.width*3)*((_page/3)-1),0) animated:NO];
        double haveToScroll=lastOffset-self.scrollView.frame.size.width*3;
        double diff=self.scrollView.contentOffset.x-haveToScroll;
      //  [self.scrollView setContentOffset:CGPointMake(self.scrollView.contentOffset.x-diff,0) animated:NO];
        xchangedvalue = self.scrollView.contentOffset.x-diff;
        _page = _page - 3;
    }
    [UIView beginAnimations:@"Scroll_view" context:nil];
    [UIView setAnimationDuration:1.0];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationTransition:UIViewAnimationOptionCurveLinear forView:self.scrollView cache:YES];
    [self.scrollView setContentOffset:CGPointMake(xchangedvalue,0) animated:NO];
    [UIView commitAnimations];
    
    //    CGRect bounds = scrollView.bounds;
    //    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"bounds"];
    //    animation.duration = 1.0;
    //    animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];
    //    animation.fromValue = [NSValue valueWithCGRect:bounds];
    //    bounds.origin.x = xchangedvalue;
    //    animation.toValue = [NSValue valueWithCGRect:bounds];
    //    [self.scrollView.layer addAnimation:animation forKey:@"bounds"];
    //    [self.scrollView.layer setNeedsDisplay];
    //    self.scrollView.bounds = bounds;
    
    self.pageControl.currentPage = _page ;
    lastOffset=self.scrollView.contentOffset.x;
}

-(void) changeTitleView:(int)type duration:(double)value
{
    //self.titleScrollView.userInteractionEnabled=NO;
    [UIView beginAnimations:@"titleScrollView" context:nil];
    [UIView setAnimationDuration:value];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationTransition:UIViewAnimationOptionCurveLinear forView:self.titleScrollView cache:YES];
    [UIView setAnimationDidStopSelector:@selector(transitionDidStop:finished:context:)];
    
    
    if (type)
    {
        double haveToScroll=lastTitleOffset+self.titleScrollView.frame.size.width*2;
        double diff=haveToScroll-self.titleScrollView.contentOffset.x;
        [self.titleScrollView setContentOffset:CGPointMake(self.titleScrollView.contentOffset.x+diff,0) animated:NO];
        _page1 = _page1 + 2;
    }
    else
    {
        // scrolling backside,
        double haveToScroll=lastTitleOffset-self.titleScrollView.frame.size.width*2;
        double diff=self.titleScrollView.contentOffset.x-haveToScroll;
        [self.titleScrollView setContentOffset:CGPointMake(self.titleScrollView.contentOffset.x-diff,0) animated:NO];
        _page1 = _page1 - 2;
    }
    self.pageControl1.currentPage = _page1 ;
    lastTitleOffset=self.titleScrollView.contentOffset.x;
    [UIView commitAnimations];
}

/*
-(void)pushGalleryController {
    NSMutableArray *photos = [[NSMutableArray alloc] init];
    
    for(NSString* link in galleryPhotos) {
        //DebugLog(@"link-->%@",link);
        [photos addObject:[MWPhoto photoWithURL:[NSURL URLWithString:link]]];
    }
    
    self.photos = photos;
    
    // Create browser
	MWPhotoBrowser *browser = [[MWPhotoBrowser alloc] initWithDelegate:self];
    browser.displayActionButton = YES;
    browser.wantsFullScreenLayout = NO;
    [browser setInitialPageIndex:0];
    btn_pressed = 0;
    [self.navigationController pushViewController:browser animated:YES];
    [self.navigationController setNavigationBarHidden:NO animated:NO];
}

#pragma mark - MWPhotoBrowserDelegate

- (NSUInteger)numberOfPhotosInPhotoBrowser:(MWPhotoBrowser *)photoBrowser {
    return _photos.count;
}

- (MWPhoto *)photoBrowser:(MWPhotoBrowser *)photoBrowser photoAtIndex:(NSUInteger)index {
    if (index < _photos.count)
        return [_photos objectAtIndex:index];
    return nil;
}
*/

////iOS 5
//- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
//{
//    return (interfaceOrientation == UIInterfaceOrientationPortrait);
//}
//////iOS6
//-(BOOL)shouldAutorotate {
//   return NO;
//}
- (NSUInteger)supportedInterfaceOrientations
{
#if __IPHONE_OS_VERSION_MAX_ALLOWED <= __IPHONE_5_1
    return UIInterfaceOrientationPortrait;
#else
    return UIInterfaceOrientationMaskPortrait;
#endif
}

- (IBAction)latestBtnPressed:(id)sender {
    [self openController:2];
}

- (IBAction)danceBtnPressed:(id)sender {
    [self openController:4];
}

- (IBAction)moviesBtnPressed:(id)sender {
    [self openController:6];
}

- (IBAction)fashionBtnPressed:(id)sender {
    [self openController:8];
}

- (IBAction)healthBtnPressed:(id)sender {
    [self openController:10];
}

- (IBAction)artBtnPressed:(id)sender {
    [self openController:12];
}

- (IBAction)galleryBtnPressed:(id)sender {
    [self openController:14];
}

- (IBAction)facebookTimeLineBtnPressed:(id)sender {
    if([MDHDAppDelegate networkavailable])
    {
        [Flurry logEvent:@"Madhuri_Website"];
        MDWebViewController *typedetailController = [[MDWebViewController alloc] initWithNibName:@"MDWebViewController" bundle:nil] ;
        typedetailController.title = @"Facebook Wall";
        typedetailController.categoryId = 3000;
        //[self.navigationController setNavigationBarHidden:NO animated:NO];
        [self.navigationController pushViewController:typedetailController animated:YES];
    } else {
        UIAlertView *alertView = [[UIAlertView alloc]
                                  initWithTitle:@"No Network Connection"
                                  message:@"Please check your internet connection and try again."
                                  delegate:self
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil];
        [alertView show];
    }
}

- (IBAction)twitterTimeLineBtnPressed:(id)sender {
    if([MDHDAppDelegate networkavailable])
    {
        [Flurry logEvent:@"Madhuri_Website"];
        MDWebViewController *typedetailController = [[MDWebViewController alloc] initWithNibName:@"MDWebViewController" bundle:nil] ;
        typedetailController.title = @"Twitter Wall";
        typedetailController.categoryId = 4000;
        //[self.navigationController setNavigationBarHidden:NO animated:NO];
        [self.navigationController pushViewController:typedetailController animated:YES];
    } else {
        UIAlertView *alertView = [[UIAlertView alloc]
                                  initWithTitle:@"No Network Connection"
                                  message:@"Please check your internet connection and try again."
                                  delegate:self
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil];
        [alertView show];
    }
}


- (IBAction)openWebsite:(id)sender {
    if([MDHDAppDelegate networkavailable])
    {
        [Flurry logEvent:@"Phonethics_Website"];
        MDWebViewController *typedetailController = [[MDWebViewController alloc] initWithNibName:@"MDWebViewController" bundle:nil] ;
        typedetailController.title = @"Phonethics.in";
        typedetailController.categoryId = 2000;
        [self.navigationController setNavigationBarHidden:NO animated:NO];
        [self.navigationController pushViewController:typedetailController animated:YES];
    } else {
        UIAlertView *alertView = [[UIAlertView alloc]
                                  initWithTitle:@"No Network Connection"
                                  message:@"Please check your internet connection and try again."
                                  delegate:self
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil];
        [alertView show];
    }
}

- (IBAction)flipViewBtnPressed:(id)sender {
    if(self.menuView.isHidden)
    {
        [Flurry logEvent:@"Navigation_Menu"];
        [self.view bringSubviewToFront:self.menuView];
        [self.view bringSubviewToFront:self.flipViewBtn];
        self.menuView.alpha = 0.0f;
        [UIView beginAnimations: @"SHOW_MENU" context:nil];
        // wait for time before begin
        [UIView setAnimationDelay:0];
        // druation of animation
        [UIView setAnimationDuration:1.0];
        [UIView setAnimationDelegate:self];
        [UIView setAnimationDidStopSelector:@selector(menutransitionDidStop:finished:context:)];
        self.menuView.alpha = 1.0f;
        [flipViewBtn setImage:[UIImage imageNamed:@"close_off"] forState:UIControlStateNormal];
        [flipViewBtn setImage:[UIImage imageNamed:@"close_on"] forState:UIControlStateHighlighted];
        [UIView commitAnimations];
        [self.menuView setHidden:FALSE];
    } else {
        self.menuView.alpha = 1.0f;
        [UIView beginAnimations: @"HIDE_MENU" context:nil];
        // wait for time before begin
        [UIView setAnimationDelay:0];
        // druation of animation
        [UIView setAnimationDuration:1.0];
        [UIView setAnimationDelegate:self];
        [UIView setAnimationDidStopSelector:@selector(menutransitionDidStop:finished:context:)];
        self.menuView.alpha = 0.0f;
        [flipViewBtn setImage:[UIImage imageNamed:@"menu_icon_off"] forState:UIControlStateNormal];
        [flipViewBtn setImage:[UIImage imageNamed:@"menu_icon_on"] forState:UIControlStateHighlighted];
        [UIView commitAnimations];
    }

}

@end
