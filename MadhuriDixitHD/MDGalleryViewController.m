//
//  MDGalleryViewController.m
//  MadhuriDixitHD
//
//  Created by Rishi on 29/01/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import "MDGalleryViewController.h"
#import "SBJson.h"

//#define GALLERY_LINK @"http://madhuridixit-nene.com/uploads/gallery.xml"
#define GALLERY_LINK @"http://madhuridixit-nene.com/wp/?json=get_page&id=301&exclude=content"

@interface MDGalleryViewController ()

@end

@implementation MDGalleryViewController
@synthesize loaderImage;
@synthesize alertView;
@synthesize photos = _photos;
@synthesize dataFilePath;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
//- (void)viewWillAppear:(BOOL)animated
//{
//	[super viewWillAppear:animated];
//    [self.navigationController setNavigationBarHidden:YES animated:NO];
//}
//- (void)viewDidAppear:(BOOL)animated {
//    [super viewDidAppear:animated];
//}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [alertView setHidden:YES];
    //self.responseIndicator.hidesWhenStopped = TRUE;
    //[self.responseIndicator startAnimating];
    
    [self rotateImage:loaderImage];
    
	// Do any additional setup after loading the view, typically from a nib.
    if(galleryPhotos == nil) {
        galleryPhotos = [[NSMutableArray alloc] init];
    }
    [self loadMore];
//    if([MDHDAppDelegate networkavailable])
//        [self startJsonRequest];
//    else
//    {
//        goBack=YES;
//        [self.loaderImage.layer removeAnimationForKey:@"360"];
//        [self.loaderImage setHidden:TRUE];
//        [self loadDataFromFile];
//    }
}

-(void) loadMore
{
    NSFileManager *filemgr = [NSFileManager defaultManager];
    
    // Get the documents directory
    NSArray *dirPaths = NSSearchPathForDirectoriesInDomains(
                                                            NSDocumentDirectory, NSUserDomainMask, YES);
    
    NSString *docsDir = [dirPaths objectAtIndex:0];
    
    // Build the path to the data file
    dataFilePath = [[NSString alloc] initWithString: [docsDir
                                                      stringByAppendingPathComponent: @"gallery.archive"]];
    
    // Check if the file already exists
    if ([filemgr fileExistsAtPath: dataFilePath])
    {
        NSMutableArray *dataArray;
        dataArray = [NSKeyedUnarchiver
                     unarchiveObjectWithFile: dataFilePath];
        
        NSDate *oldDate =  (NSDate *)[dataArray objectAtIndex:0];
        NSDate *currentDate =  [NSDate date];

        galleryPhotos   = [dataArray objectAtIndex:1];

        DebugLog(@"-----oldDate %@ --galleryPhotos %@--",oldDate,galleryPhotos);
        
        NSCalendar *calendar = [NSCalendar currentCalendar];
        NSInteger comps = (NSDayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit);
        
        NSDateComponents *date1Components = [calendar components:comps
                                                        fromDate: oldDate];
        NSDateComponents *date2Components = [calendar components:comps
                                                        fromDate: [NSDate date]];
        
        oldDate = [calendar dateFromComponents:date1Components];
        currentDate = [calendar dateFromComponents:date2Components];
        
        DebugLog(@"-----oldDate %@ --currentDate %@--",oldDate,currentDate);

        NSComparisonResult result = [oldDate compare:currentDate];
        DebugLog(@"-----result %d ----",result);
        if (result == NSOrderedAscending)
        {
            DebugLog(@"oldDate < currentDate"); // result = -1
        }
        else if (result == NSOrderedDescending)
        {
            DebugLog(@"oldDate > currentDate"); // result = 1
        }
        else {
            DebugLog(@"oldDate == currentDate"); // result = 0
        }
        if (galleryPhotos.count != 0 && result == 0)
        {
            [self.loaderImage.layer removeAnimationForKey:@"360"];
            [self.loaderImage setHidden:TRUE];
            goBack=YES;
            [self pushGalleryController];
            return;
        }
        else
        {
            [self startJsonRequest];
        }
    }
    else
    {
        DebugLog(@"file not exists");
        [self startJsonRequest];
    }
}

- (void)rotateImage:(UIImageView *)lview
{
    CABasicAnimation *fullRotation = [CABasicAnimation animationWithKeyPath:@"transform.rotation"];
    fullRotation.fromValue = [NSNumber numberWithFloat:0];
    fullRotation.toValue = [NSNumber numberWithFloat:((360*M_PI)/180)];
    fullRotation.duration = 1.00;
    fullRotation.repeatCount = HUGE_VALF;
    fullRotation.removedOnCompletion = YES;
    [lview.layer addAnimation:fullRotation forKey:@"360"];
    [lview.layer setSpeed:0.5];
    
    
}
-(void) startJsonRequest
{
    if([MDHDAppDelegate networkavailable])
    {
        goBack=NO;
        [self.loaderImage setHidden:NO];
        [self rotateImage:loaderImage];
//        NSURLRequest *urlRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:GALLERY_LINK] cachePolicy:YES timeoutInterval:30.0];
        NSURLRequest *urlRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:GALLERY_LINK]];
        conn=[[NSURLConnection alloc] initWithRequest:urlRequest delegate:self startImmediately:YES];
    }
    else
    {
        goBack=YES;
        [self.loaderImage.layer removeAnimationForKey:@"360"];
        [self.loaderImage setHidden:TRUE];
        UIAlertView *errorView = [[UIAlertView alloc]
                                  initWithTitle:@"No Network Connection"
                                  message:@"Please check your internet connection and try again."
                                  delegate:self
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil];
        [errorView show];
    }
}
#pragma mark NSURLConnection Delegate Methods

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
	
	if ([response isKindOfClass:[NSHTTPURLResponse class]])
	{
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
        
		status = [httpResponse statusCode];
        DebugLog(@"status = %d ",status);
	}
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
	
	if(responseData==nil)
	{
		responseData = [[NSMutableData alloc] initWithLength:0];
	}
	[responseData appendData:data];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    
    [self.loaderImage.layer removeAnimationForKey:@"360"];
    [self.loaderImage setHidden:TRUE];
    [self loadDataFromWeb];
    goBack=YES;
    [alertView setHidden:YES];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    
    [self.loaderImage.layer removeAnimationForKey:@"360"];
    [self.loaderImage setHidden:TRUE];
    //[self loadDataFromFile];
    //[self loadMore];
    goBack=YES;
    [alertView setHidden:YES];
}

-(void)loadDataFromFile
{
    NSString *responseString = [[NSString alloc] initWithString:[MDHDAppDelegate getTextFromFile:@"gallery"]];
    if(![responseString isEqualToString:@""])
    {
        if(galleryPhotos != nil)
        {
            [galleryPhotos removeAllObjects];
        }
        [self parseData:responseString];
    }
    else
    {
        //DebugLog(@"\n null data\n");
        UIAlertView *errorView = [[UIAlertView alloc]
                                  initWithTitle:@"No Network Connection"
                                  message:@"Please check your internet connection and try again."
                                  delegate:self
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil];
        [errorView show];
    }
    responseString = nil;
    self->responseData = nil;
}

-(void)loadDataFromWeb
{
    NSString *responseString = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
    [MDHDAppDelegate writeToTextFile:responseString name:@"gallery"];
    [self parseData:responseString];
    responseString = nil;
    self->responseData = nil;
}

-(void)parseData:(NSString *)dataString
{
    //Get json value
    NSDictionary* mainDict = (NSDictionary*)[dataString JSONValue];
    
    if (status == 200)
    {
        //Get address dictionary
        NSDictionary* pageDict = [mainDict objectForKey:@"page"];
        NSArray *attachmentArray    =   [pageDict objectForKey:@"attachments"];
        // DebugLog(@"attachmentArray : %@",attachmentArray);

        if (galleryPhotos != nil) {
            [galleryPhotos removeAllObjects];
        }
        for(NSDictionary *attachmentDict in attachmentArray)
        {
            //Get phone number array
            NSDictionary* imagesDict        =   [attachmentDict objectForKey:@"images"];
            NSDictionary* thumbnailDict = nil;
            if ([imagesDict objectForKey:@"full"] != nil)
            {
                thumbnailDict     =   [imagesDict objectForKey:@"full"];
            }
            else if ([imagesDict objectForKey:@"medium"] != nil) {
                DebugLog(@"imagesDict medium");
                thumbnailDict     =   [imagesDict objectForKey:@"medium"];
            }
            if(thumbnailDict != nil){
                NSString * thumbnailImageUrl    =   [thumbnailDict objectForKey:@"url"];
                DebugLog(@"imagesDict %@ fullImageUrl %@",imagesDict,thumbnailImageUrl);
                if (thumbnailImageUrl != nil && ![thumbnailImageUrl isEqualToString:@""]) {
                    [galleryPhotos addObject:thumbnailImageUrl];
                }
            }
        }
        DebugLog(@"Done parsing.. array %@",galleryPhotos);
        if (galleryPhotos.count > 0) {
            [NSKeyedArchiver archiveRootObject:[[NSMutableArray alloc] initWithObjects:[NSDate date],galleryPhotos, nil] toFile:dataFilePath];
            [self pushGalleryController];
        }
    }
}
-(BOOL)isMember:(NSString *)currentUrl
{
    for (NSString *oldUrl in galleryPhotos)
    {
        if([oldUrl isEqualToString:currentUrl])
            return YES;
    }
    return NO;
}
- (IBAction)alertActionMethod:(id)sender {
    
    UIButton *button=(UIButton *)sender;
    switch (button.tag) {
        case 0:
        {
            [self backAnimation];
            [conn cancel];
        }
            break;
            
        case 1:
        {
            [alertView setHidden:YES];
        }
            break;
        default:
            break;
    }
}

-(void)backAnimation
{
    //[self stopSectionAudio];
    //[Flurry endTimedEvent:@"Gallery_Session" withParameters:nil];
    [UIView  transitionWithView:self.navigationController.view duration:1.0  options:UIViewAnimationOptionTransitionFlipFromRight
                     animations:^(void) {
                         BOOL oldState = [UIView areAnimationsEnabled];
                         [UIView setAnimationsEnabled:NO];
                         [self.navigationController popViewControllerAnimated:YES];
                         [UIView setAnimationsEnabled:oldState];
                     }
                     completion:nil];
}

- (void)viewDidUnload
{
    [self setLoaderImage:nil];
    [self setAlertView:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}
-(void)pushGalleryController {
    NSMutableArray *photos = [[NSMutableArray alloc] init];
    
    for(NSString* link in galleryPhotos) {
        //DebugLog(@"link-->%@",link);
        [photos addObject:[MWPhoto photoWithURL:[NSURL URLWithString:link]]];
    }
    
    self.photos = photos;
    
    // Create browser
	MWPhotoBrowser *browser = [[MWPhotoBrowser alloc] initWithDelegate:self];
    browser.displayActionButton = YES;
    browser.wantsFullScreenLayout = NO;
    [browser setInitialPageIndex:0];
    //btn_pressed = 0;
    [UIView  transitionWithView:self.navigationController.view duration:1.0  options:UIViewAnimationOptionTransitionNone
                     animations:^(void) {
                         BOOL oldState = [UIView areAnimationsEnabled];
                         [UIView setAnimationsEnabled:NO];
                         [self.navigationController pushViewController:browser animated:NO];
                         [UIView setAnimationsEnabled:oldState];
                     }
                     completion:nil]; 
    //[self.navigationController pushViewController:browser animated:YES];
    [self.navigationController setNavigationBarHidden:NO animated:NO];
}

#pragma mark - MWPhotoBrowserDelegate

- (NSUInteger)numberOfPhotosInPhotoBrowser:(MWPhotoBrowser *)photoBrowser {
    return _photos.count;
}

- (MWPhoto *)photoBrowser:(MWPhotoBrowser *)photoBrowser photoAtIndex:(NSUInteger)index {
    if (index < _photos.count)
        return [_photos objectAtIndex:index];
    return nil;
}

- (NSUInteger)supportedInterfaceOrientations{
#if __IPHONE_OS_VERSION_MAX_ALLOWED <= __IPHONE_5_1
    return UIInterfaceOrientationPortrait;
#else
    return UIInterfaceOrientationMaskPortrait;
#endif
}
- (IBAction)backActionMethod:(id)sender {
    if (goBack)
    {
        [self backAnimation];
    }
    else
    {
        [alertView setHidden:NO];
    }
}
@end
