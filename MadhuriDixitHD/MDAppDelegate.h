//
//  MDAppDelegate.h
//  MadhuriDixitHD
//
//  Created by Rishi on 15/01/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Reachability.h"
#import <FacebookSDK/FacebookSDK.h>

#define MDHDAppDelegate (MDAppDelegate*)[[UIApplication sharedApplication] delegate] 

#define titlenotificationName  @"MadhuriTitleNotification"
#define titlenotificationKey  @"ButtonPressed"

extern NSString *const SCSessionStateChangedNotification;

@class MDMainViewController;
@class SCViewController;

@interface MDAppDelegate : UIResponder <UIApplicationDelegate> {
    Reachability* internetReach;
    Boolean networkavailable;
}
@property(nonatomic) Boolean networkavailable;
@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) UINavigationController *navigationController;
@property (strong, nonatomic) MDMainViewController *viewController;

- (NSString *)filterHTMLCodes:(NSString *)String;
-(void) writeToTextFile:(NSString *)lcontent name:(NSString *)lname;
-(NSString *) getTextFromFile:(NSString *)lname;
- (void)removeFile:(NSString*)lname;
- (void)saveImage: (UIImage*)image name:(int)lname;
- (void)saveNetworkStatus:(int)lval;
- (int)getNetworkStatus;
- (BOOL)openSessionWithAllowLoginUI:(BOOL)allowLoginUI;
@end
