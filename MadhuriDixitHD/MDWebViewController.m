//
//  MDWebViewController.m
//  MadhuriDixitHD
//
//  Created by Sagar Mody on 20/01/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import "MDWebViewController.h"
#import "MDLatestViewController.h"
#import "MadhuriCategoryPost.h"
#import "MDDanceViewController.h"
#import "MDArtViewController.h"
#import "MDFashionViewController.h"
#import "MDHealthBeautyViewController.h"
#import <Twitter/Twitter.h>
#import "SCViewController.h"
#import <Social/Social.h>

#define PHONETHICS_WEBSITE_LINK @"http://www.phonethics.in/"
#define FACEBOOK_LINK @"https://www.facebook.com/MadhuriDixitNene"
#define TWITTER_LINK @"http://mobile.twitter.com/madhuridixit"

@interface MDWebViewController ()

@end

@implementation MDWebViewController
@synthesize contentview;
@synthesize navBarImg = _navBarImg;
@synthesize loader_image = _loader_image;
@synthesize facebookshareBtn = _facebookshareBtn;
@synthesize backBtn = _backBtn;
@synthesize barbackBtn = _barbackBtn;
@synthesize barfwdBtn = _barfwdBtn;
@synthesize barrefreshBtn = _barrefreshBtn;
@synthesize webviewtoolBar = _webviewtoolBar;
@synthesize categoryId=_categoryId;
@synthesize posttitle=_posttitle;
@synthesize postLink=_postLink;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    DebugLog(@"self.categoryid %d",self.categoryId);
    [_barbackBtn setEnabled:FALSE];
    [_barfwdBtn setEnabled:FALSE];
    [_barrefreshBtn setEnabled:FALSE];
    [_webviewtoolBar setHidden:TRUE];
    UIBarButtonItem *shareButton = [[UIBarButtonItem alloc] initWithTitle:@"Share" style:UIBarButtonItemStylePlain target:self action:@selector(save_Clicked:)];
    self.navigationItem.rightBarButtonItem = shareButton;
    
    NSString *htmlString;
    
    
    if([self.title isEqualToString:@"Latest"]){
        [self.navBarImg setImage:[UIImage imageNamed:@"bar_latest.png"]];
        MDLatestViewController *parent = [self.navigationController.viewControllers objectAtIndex:1];
        if(parent != nil)
        {
            MadhuriCategoryPost *catObj = (MadhuriCategoryPost *)[parent.latestArray objectAtIndex:self.categoryId];
            htmlString = [[NSString alloc] initWithString:catObj.categoryPostContent];
            self.posttitle = [[NSString alloc] initWithString:catObj.categoryPostTitle];
            self.postLink = [[NSString alloc] initWithString:catObj.categoryPostUrl];
        }
        parent = nil;
    } else if([self.title isEqualToString:@"Dance"]) {
        [self.navBarImg setImage:[UIImage imageNamed:@"bar_dance.png"]];
        MDDanceViewController *parent = [self.navigationController.viewControllers objectAtIndex:1];
        if(parent != nil)
        {
            MadhuriCategoryPost *catObj = (MadhuriCategoryPost *)[parent.danceArray objectAtIndex:self.categoryId];
            htmlString = [[NSString alloc] initWithString:catObj.categoryPostContent];
            self.posttitle = [[NSString alloc] initWithString:catObj.categoryPostTitle];
            self.postLink = [[NSString alloc] initWithString:catObj.categoryPostUrl];
        }
        parent = nil;
    } else if([self.title isEqualToString:@"Art"]) {
        [self.navBarImg setImage:[UIImage imageNamed:@"bar_art.png"]];
        MDArtViewController *parent = [self.navigationController.viewControllers objectAtIndex:1];
        if(parent != nil)
        {
            MadhuriCategoryPost *catObj = (MadhuriCategoryPost *)[parent.artArray objectAtIndex:self.categoryId];
            htmlString = [[NSString alloc] initWithString:catObj.categoryPostContent];
            self.posttitle = [[NSString alloc] initWithString:catObj.categoryPostTitle];
            self.postLink = [[NSString alloc] initWithString:catObj.categoryPostUrl];
        }
        parent = nil;
    } else if([self.title isEqualToString:@"Fashion"]) {
        [self.navBarImg setImage:[UIImage imageNamed:@"bar_fashion.png"]];
        MDFashionViewController *parent = [self.navigationController.viewControllers objectAtIndex:1];
        if(parent != nil)
        {
            MadhuriCategoryPost *catObj = (MadhuriCategoryPost *)[parent.fashionArray objectAtIndex:self.categoryId];
            htmlString = [[NSString alloc] initWithString:catObj.categoryPostContent];
            self.posttitle = [[NSString alloc] initWithString:catObj.categoryPostTitle];
            self.postLink = [[NSString alloc] initWithString:catObj.categoryPostUrl];
        }
        parent = nil;
    } else if([self.title isEqualToString:@"Health & Beauty"]) {
        [self.navBarImg setImage:[UIImage imageNamed:@"bar_health.png"]];
        MDHealthBeautyViewController *parent = [self.navigationController.viewControllers objectAtIndex:1];
        if(parent != nil)
        {
            MadhuriCategoryPost *catObj = (MadhuriCategoryPost *)[parent.healthbeautyArray objectAtIndex:self.categoryId];
            htmlString = [[NSString alloc] initWithString:catObj.categoryPostContent];
            self.posttitle = [[NSString alloc] initWithString:catObj.categoryPostTitle];
            self.postLink = [[NSString alloc] initWithString:catObj.categoryPostUrl];
        }
        parent = nil;
    } else if(self.categoryId == 1000) {
        [self.facebookshareBtn setHidden:TRUE];
        [self.navBarImg setImage:[UIImage imageNamed:@"bar_movies.png"]];
        htmlString = [[NSString alloc] initWithString:self.posttitle];
    } else if(self.categoryId == 2000) {
        [self.facebookshareBtn setHidden:TRUE];
        [self.navBarImg setHidden:TRUE];
        [self.backBtn setHidden:TRUE];
        self.navigationItem.rightBarButtonItem = nil;
        self.contentview.frame = CGRectMake(0, 0, self.contentview.frame.size.width, self.contentview.frame.size.height+self.navBarImg.frame.size.height);
        //[self.navBarImg setImage:[UIImage imageNamed:@"bar_latest.png"]];
        NSURL *url = [NSURL URLWithString:PHONETHICS_WEBSITE_LINK];
        NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
        [contentview setDelegate:self];
        [contentview loadRequest:requestObj];
        [_webviewtoolBar setHidden:FALSE];
        return;
    } else if(self.categoryId == 3000) {         //FACEBOOK
        [self.facebookshareBtn setHidden:TRUE];
        //[self.navBarImg setHidden:TRUE];
        //[self.backBtn setHidden:TRUE];
        self.navigationItem.rightBarButtonItem = nil;
        //self.contentview.frame = CGRectMake(0, 0, self.contentview.frame.size.width, self.contentview.frame.size.height+self.navBarImg.frame.size.height);
        [self.navBarImg setImage:[UIImage imageNamed:@"bar_facebook.png"]];
        NSURL *url = [NSURL URLWithString:FACEBOOK_LINK];
        NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
        [contentview setDelegate:self];
        [contentview loadRequest:requestObj];
        [_webviewtoolBar setHidden:FALSE];
        return;
    } else if(self.categoryId == 4000) {        //TWITTER
        [self.facebookshareBtn setHidden:TRUE];
        //[self.navBarImg setHidden:TRUE];
        //[self.backBtn setHidden:TRUE];
        self.navigationItem.rightBarButtonItem = nil;
        //self.contentview.frame = CGRectMake(0, 0, self.contentview.frame.size.width, self.contentview.frame.size.height+self.navBarImg.frame.size.height);
        [self.navBarImg setImage:[UIImage imageNamed:@"bar_twitter.png"]];
        NSURL *url = [NSURL URLWithString:TWITTER_LINK];
        NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
        [contentview setDelegate:self];
        [contentview loadRequest:requestObj];
        return;
    } else if(self.categoryId == 5000) {        //MDWEBSITE
        [self.facebookshareBtn setHidden:TRUE];
        [self.navBarImg setHidden:TRUE];
        [self.backBtn setHidden:TRUE];
        self.navigationItem.rightBarButtonItem = nil;
        self.contentview.frame = CGRectMake(0, 0, self.contentview.frame.size.width, self.contentview.frame.size.height+self.navBarImg.frame.size.height);
        //[self.navBarImg setImage:[UIImage imageNamed:@"bar_latest.png"]];
        NSURL *url = [NSURL URLWithString:MD_WEBSITE_LINK];
        NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
        [contentview setDelegate:self];
        [contentview loadRequest:requestObj];
        return;
    }
    
    NSString *css = [NSString stringWithFormat:@"<!DOCTYPE html><head><meta charset=\"UTF-8\"/><meta name=\"viewport\" content=\"width=device-width\" /><title>Madhuri Dixit-Nene</title><link rel=\"stylesheet\" type=\"text/css\" media=\"all\" href=\"http://madhuridixit-nene.com/wp/wp-content/themes/twentyeleven/style.css\" /><style type=\"text/css\">.recentcomments a{display:inline !important;padding:0 !important;margin:0 !important;}</style><style type=\"text/css\">p {padding:5px !important;margin:5px !important;}</style></head><body class=\"single single-post postid-1606 single-format-standard single-author singular two-column right-sidebar\"><div class=\"entry-content\">%@</div></body></html>",htmlString];
    
    [contentview loadHTMLString:css baseURL:nil];

}

- (IBAction)share_clicked:(id)sender {
    [self showActionsheet];
}

-(void)showActionsheet
{
	UIActionSheet *uiActionSheetP= [[UIActionSheet alloc]
                                    initWithTitle: @""
                                    delegate:self
                                    cancelButtonTitle:@"CANCEL"
                                    destructiveButtonTitle:nil
                                    otherButtonTitles:@"Facebook",@"Twitter", nil];
	
	uiActionSheetP.actionSheetStyle = UIActionSheetStyleBlackTranslucent;
	[uiActionSheetP showInView:self.view];
    
}

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    //NSString *html = [contentview stringByEvaluatingJavaScriptFromString:@"document.body.innerHTML"];
	if (buttonIndex == 0) {
        [self facebookTapped];
    } else if (buttonIndex == 1) {
        [self tweetTapped];
    }
	
}

- (void)facebookTapped {
    if (SYSTEM_VERSION_LESS_THAN(@"6.0"))
    {
        SCViewController *facebookViewComposer = [[SCViewController alloc] initWithNibName:@"SCViewController" bundle:nil];
        facebookViewComposer.title = @"FACEBOOK";
        facebookViewComposer.FBtitle = self.posttitle;
        facebookViewComposer.FBtLink = self.postLink;
        facebookViewComposer.modalPresentationStyle = UIModalPresentationCurrentContext;
        [self.navigationController presentModalViewController:facebookViewComposer animated:YES];
    }
    else
    {
        DebugLog(@"Got ios 6");
//        NSData *imageData = [[NSData alloc] initWithContentsOfURL:[NSURL URLWithString:@"http://madhuridixit-nene.com/uploads/Madhuri_App_Icon.png"]];
        SLComposeViewController *socialComposer = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
        [socialComposer addURL:[NSURL URLWithString:self.postLink]];
//        [socialComposer setTitle:self.posttitle];
//        [socialComposer setInitialText:self.posttitle];
//        [socialComposer addImage:[UIImage imageWithData:imageData]];
        
        [socialComposer setCompletionHandler:^(SLComposeViewControllerResult result){
            switch (result) {
                case SLComposeViewControllerResultCancelled:
                    DebugLog(@"SLComposeViewControllerResultCancelled");
                    break;
                case SLComposeViewControllerResultDone:
                    DebugLog(@"SLComposeViewControllerResultDone");
                    break;
                default:
                    DebugLog(@"SLComposeViewControllerResultFailed");
                    break;
            }
        }];
        
        [self.navigationController presentModalViewController:socialComposer animated:YES];
    }
}




- (void)tweetTapped {
    
    if ([TWTweetComposeViewController canSendTweet])
    {
        TWTweetComposeViewController *tweetSheet = [[TWTweetComposeViewController alloc] init];
        if(self.postLink != nil && self.posttitle != nil) {
            [tweetSheet setInitialText:[NSString stringWithFormat:@"%@ \n %@",self.posttitle , self.postLink]];
        } else {
            [tweetSheet setInitialText:@"Tweeting from Madhuri Dixit iOS App!!!"];
        }
        [self presentModalViewController:tweetSheet animated:YES];
    }
    else
    {
        UIAlertView *alertView = [[UIAlertView alloc]
                                  initWithTitle:@"Sorry"
                                  message:@"You can't send a tweet right now, make sure your device has an internet connection and you have at least one Twitter account setup."
                                  delegate:self
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil];
        [alertView show];
    }
}


- (void)viewDidUnload
{
    [self setContentview:nil];
    [self setNavBarImg:nil];
    [self setLoader_image:nil];
    [self setFacebookshareBtn:nil];
    [self setBackBtn:nil];
    [self setBarbackBtn:nil];
    [self setBarfwdBtn:nil];
    [self setBarrefreshBtn:nil];
    [self setWebviewtoolBar:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)rotateImage:(UIImageView *)lview
{
    CABasicAnimation *fullRotation = [CABasicAnimation animationWithKeyPath:@"transform.rotation"];
    fullRotation.fromValue = [NSNumber numberWithFloat:0];
    fullRotation.toValue = [NSNumber numberWithFloat:((360*M_PI)/180)];
    fullRotation.duration = 1.00;
    fullRotation.repeatCount = HUGE_VALF;
    fullRotation.removedOnCompletion = YES;
    [lview.layer addAnimation:fullRotation forKey:@"360"];
    [lview.layer setSpeed:0.5];
    
    
}
- (void)webViewDidStartLoad:(UIWebView *)webView
{
    [_barrefreshBtn setEnabled:FALSE];
    [self.loader_image setHidden:FALSE];
    [self rotateImage:self.loader_image];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    if(contentview.canGoBack)
    {
        [_barbackBtn setEnabled:TRUE];
    } else {
        [_barbackBtn setEnabled:FALSE];
    }
    if(contentview.canGoForward)
    {
        [_barfwdBtn setEnabled:TRUE];
    } else {
        [_barfwdBtn setEnabled:FALSE];
    }
    [_barrefreshBtn setEnabled:TRUE];
    [self.loader_image.layer removeAnimationForKey:@"360"];
    [self.loader_image setHidden:TRUE];
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    [_barrefreshBtn setEnabled:TRUE];
    [self.loader_image.layer removeAnimationForKey:@"360"];
    [self.loader_image setHidden:TRUE];
}

//iOS 5
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    if (self.categoryId == 3000 || self.categoryId == 4000 || self.categoryId == 2000 ) {
        return NO;
    }
    return YES;
}
//iOS6
- (BOOL)shouldAutorotate {
    if (self.categoryId == 3000 || self.categoryId == 4000 || self.categoryId == 2000 ) {
        return NO;
    }
    return YES;
}
- (NSUInteger)supportedInterfaceOrientations{
    if (self.categoryId == 3000 || self.categoryId == 4000 || self.categoryId == 2000 )
    {
        #if __IPHONE_OS_VERSION_MAX_ALLOWED <= __IPHONE_5_1
                return UIInterfaceOrientationPortrait;
        #else
                return UIInterfaceOrientationMaskPortrait;
        #endif
    }
    else
    {
        #if __IPHONE_OS_VERSION_MAX_ALLOWED <= __IPHONE_5_1
            return (UIInterfaceOrientationPortrait | UIDeviceOrientationLandscapeRight | UIDeviceOrientationLandscapeLeft);
        #else
            return UIInterfaceOrientationMaskAll;
        #endif
    }
}
- (IBAction)backBtnPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)webviewbackBtnPressed:(id)sender {
    [contentview goBack];
}

- (IBAction)webviewfwdBtnPressed:(id)sender {
    [contentview goForward];
}

- (IBAction)webviewrefreshBtnPressed:(id)sender {
    [contentview reload];
}


@end
