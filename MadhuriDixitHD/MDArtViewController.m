//
//  MDArtViewController.m
//  MadhuriDixitHD
//
//  Created by Rishi on 18/01/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import "MDArtViewController.h"
#import "MDAppDelegate.h"
#import "MadhuriCategoryPost.h"
#import "MDWebViewController.h"
#import "MadhuriMovies.h"
#import "SBJson.h"
#import <SDWebImage/UIImageView+WebCache.h>

//#define ART_LINK @"http://madhuridixit-nene.com/wp/?json=get_category_posts&slug=art&count=-1"

//--Load more
#define ART_LINK(PAGE) [NSString stringWithFormat:@"http://madhuridixit-nene.com/wp/?json=get_category_posts&slug=art&&page=%d",PAGE]
//

@interface MDArtViewController ()

@end

@implementation MDArtViewController
@synthesize artTableView;
@synthesize loaderImage;
@synthesize alertView;
@synthesize artArray;
@synthesize textPull, textRelease, textLoading, refreshHeaderView, refreshLabel, refreshArrow, refreshSpinner;
@synthesize dataFilePath;
@synthesize footerView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        [self setupStrings];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    showFooterView = NO;
    [self addPullToRefreshHeader];
    [alertView setHidden:YES];
    //self.responseIndicator.hidesWhenStopped = TRUE;
    //[self.responseIndicator startAnimating];
    
    [self rotateImage:loaderImage];
    
	// Do any additional setup after loading the view, typically from a nib.
    if(artArray == nil) {
        artArray = [[NSMutableArray alloc] init];
    }
    [self loadMore];
/*    else {
        [self.loaderImage.layer removeAnimationForKey:@"360"];
        [self.loaderImage setHidden:TRUE];
        goBack=YES;
        return;
        //[artArray removeAllObjects];
    }
    
    
    if([MDHDAppDelegate networkavailable])
        [self startJsonRequest];
    else
    {
        goBack=YES;
        [self.loaderImage.layer removeAnimationForKey:@"360"];
        [self.loaderImage setHidden:TRUE];
        [self loadDataFromFile];
    }
*/
}
-(void) loadMore
{
    NSFileManager *filemgr = [NSFileManager defaultManager];
    
    // Get the documents directory
    NSArray *dirPaths = NSSearchPathForDirectoriesInDomains(
                                                            NSDocumentDirectory, NSUserDomainMask, YES);
    
    NSString *docsDir = [dirPaths objectAtIndex:0];
    
    // Build the path to the data file
    dataFilePath = [[NSString alloc] initWithString: [docsDir
                                                      stringByAppendingPathComponent: @"art.archive"]];
    
    // Check if the file already exists
    if ([filemgr fileExistsAtPath: dataFilePath])
    {
        artArray = [NSKeyedUnarchiver
                       unarchiveObjectWithFile: dataFilePath];
        DebugLog(@"artArray %@",artArray);
        if (artArray.count == 0) {
            [self pageRequest:1];
        }
        else
        {
            showFooterView = YES;
            [self.loaderImage.layer removeAnimationForKey:@"360"];
            [self.loaderImage setHidden:TRUE];
            goBack=YES;
            return;
        }
    }
    else
    {
        DebugLog(@"file not exists");
        [self pageRequest:1];
    }
}
-(void)pageRequest:(int)pageNum
{
    if([MDHDAppDelegate networkavailable])
    {
        [self startJsonRequest:ART_LINK(pageNum)];
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setInteger:pageNum forKey:@"art"];
        [defaults synchronize];
    } else
    {
        goBack=YES;
        [self.loaderImage.layer removeAnimationForKey:@"360"];
        [self.loaderImage setHidden:TRUE];
        [self stopLoading];
        UIAlertView *errorView = [[UIAlertView alloc]
                                  initWithTitle:@"No Network Connection"
                                  message:@"Please check your internet connection and try again."
                                  delegate:self
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil];
        [errorView show];
        
    }
}

- (void)rotateImage:(UIImageView *)lview
{
    CABasicAnimation *fullRotation = [CABasicAnimation animationWithKeyPath:@"transform.rotation"];
    fullRotation.fromValue = [NSNumber numberWithFloat:0];
    fullRotation.toValue = [NSNumber numberWithFloat:((360*M_PI)/180)];
    fullRotation.duration = 1.00;
    fullRotation.repeatCount = HUGE_VALF;
    fullRotation.removedOnCompletion = YES;
    [lview.layer addAnimation:fullRotation forKey:@"360"];
    [lview.layer setSpeed:0.5];
    
    
}
-(void) startJsonRequest:(NSString *) postlink
{
    goBack=NO;
    [self.loaderImage setHidden:NO];
    [self rotateImage:loaderImage];
    isLoading = YES;
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:postlink] cachePolicy:YES timeoutInterval:30.0];
    conn=[[NSURLConnection alloc] initWithRequest:urlRequest delegate:self startImmediately:YES];
}
#pragma mark NSURLConnection Delegate Methods

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
	
	if ([response isKindOfClass:[NSHTTPURLResponse class]])
	{
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
        
		status = [httpResponse statusCode];
        //DebugLog(@"status = %d ",status);
	}
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
	
	if(responseData==nil)
	{
		responseData = [[NSMutableData alloc] initWithLength:0];
	}
	
	[responseData appendData:data];
    
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    
    //[self logData];
    
    //  [self.responseIndicator stopAnimating];
    [self.loaderImage.layer removeAnimationForKey:@"360"];
    [self.loaderImage setHidden:TRUE];
    [self loadDataFromWeb];
    goBack=YES;
    [alertView setHidden:YES];
    
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    
    // [self.responseIndicator stopAnimating];
    [self.loaderImage.layer removeAnimationForKey:@"360"];
    [self.loaderImage setHidden:TRUE];
    //[self loadDataFromFile];
    [self loadMore];
    goBack=YES;
    UIButton *loadBtn = (UIButton *)[footerView viewWithTag:99];
    loadBtn.enabled = YES;
    [alertView setHidden:YES];
    if(isLoading)
    {
        [self stopLoading];
    }
}
- (void) logData
{
    if(responseData != NULL)
	{
		NSString *result = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
		//DebugLog(@"\n result:%@\n\n", result);
    }
}

-(void)loadDataFromFile
{
    NSString *responseString = [[NSString alloc] initWithString:[MDHDAppDelegate getTextFromFile:@"art"]];
    //[self logData];
    if(![responseString isEqualToString:@""])
    {
        if(isLoading && artArray != nil)
        {
            [artArray removeAllObjects];
        }
        [self parseData:responseString];
        [artTableView reloadData];
    }
    else
    {
        //DebugLog(@"\n null data\n");
        UIAlertView *errorView = [[UIAlertView alloc]
                                  initWithTitle:@"No Network Connection"
                                  message:@"Please check your internet connection and try again."
                                  delegate:self
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil];
        [errorView show];
    }
    responseString = nil;
    self->responseData = nil;
}

-(void)loadDataFromWeb
{
    NSString *responseString = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
    [MDHDAppDelegate writeToTextFile:responseString name:@"art"];
    if(isLoading && artArray != nil)
    {
        [self stopLoading];
      //  [artArray removeAllObjects];
    }
    [self parseData:responseString];
    [artTableView reloadData];
    responseString = nil;
    self->responseData = nil;
    
}

-(void)parseData:(NSString *)dataString
{
    //Get json value
    NSDictionary* mainDict = (NSDictionary*)[dataString JSONValue];
//    NSString * firstname = [mainDict objectForKey:@"status"];
//	NSString * lastname = [mainDict objectForKey:@"count"];
//    NSString * age = [mainDict objectForKey:@"pages"];
    //DebugLog(@" %@ , %@ , %@ ",firstname,lastname,age);
    NSString * count = [mainDict objectForKey:@"count"];
    NSString * pages = [mainDict objectForKey:@"pages"];
    DebugLog(@"count %@",count);
    if ([count intValue] == 0)
    {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setInteger:[pages intValue] forKey:@"art"];
        [defaults synchronize];
        showFooterView = NO;
    }
    else
    {
        showFooterView = YES;
        //Get address dictionary
        NSDictionary* maincategory = [mainDict objectForKey:@"category"];
        NSArray *keys = [maincategory allKeys]; // the keys for your dictionary
        NSEnumerator *e = [keys objectEnumerator];
        NSString *string;
        while (string = [e nextObject]) {  //Loop through all the values in dictionary
            // do something with object
            NSString * value = [maincategory objectForKey:string];
            //DebugLog(@"%@ = %@", string, value);
        }
        
        //Get phone number array
        NSArray* postsArray = [mainDict objectForKey:@"posts"];
        
        for(NSDictionary* phone in postsArray){  //Loop through all the dictionaries in an array
            NSString * lid = [phone objectForKey:@"id"];
            NSString * type = [phone objectForKey:@"type"];
            NSString * slug = [phone objectForKey:@"slug"];
            NSString * url = [phone objectForKey:@"url"];
            NSString * lstatus = [phone objectForKey:@"status"];
            NSString * title = [[phone objectForKey:@"title"] stringByConvertingHTMLToPlainText];//[MDHDAppDelegate  filterHTMLCodes:[phone objectForKey:@"title"]];
            NSString * title_plain = [phone objectForKey:@"title_plain"];
            NSString * content = [phone objectForKey:@"content"];
            NSString * excerpt = [phone objectForKey:@"excerpt"];
            NSString * date = [phone objectForKey:@"date"];
            NSString * modified = [phone objectForKey:@"modified"];
            //NSString * thumbUrl = [phone objectForKey:@"thumbnail"];
            //DebugLog(@" id %@ , type %@, slug %@, url %@, status %@, title %@, title_plain %@, excerpt %@, date %@, modified %@",lid,type,slug,url,lstatus,title,title_plain,excerpt,date,modified);
            //DebugLog(@" content %@" , content);
            
            NSArray* attachmentArray = [phone objectForKey:@"attachments"];
            NSString * thumbUrl;
            if(attachmentArray.count >= 1) {
                NSDictionary* attachment= [attachmentArray objectAtIndex:attachmentArray.count-1];
                NSDictionary* imagesDict =  [attachment objectForKey:@"images"];
                NSDictionary* thumbnailDict =  [imagesDict objectForKey:@"thumbnail"];
                thumbUrl = [thumbnailDict objectForKey:@"url"];
            } else {
                thumbUrl = @"";
            }
            
            // Create a new category object with the data from the parser
            MadhuriCategoryPost *catObj = [[MadhuriCategoryPost alloc] init];
            catObj.categoryPostId = [lid integerValue];
            catObj.categoryPostType = type;
            catObj.categoryPostSlug = slug;
            catObj.categoryPostUrl = [NSString stringWithFormat:@"%@/art/post/%@",MD_WEBSITE_LINK,slug];
            catObj.categoryPostStatus = lstatus;
            catObj.categoryPostTitle = title;
            catObj.categoryPostTitlePlain = title_plain;
            catObj.categoryPostContent = content;
            catObj.categoryPostExcerpt = excerpt;
            catObj.categoryPostDate = date;
            catObj.categoryPostModified = modified;
            catObj.categoryPostThumbnail = thumbUrl;
            if(![self isMember:catObj.categoryPostId])
                [artArray addObject:catObj];
            catObj = nil;
        }
        //Load more
        [NSKeyedArchiver archiveRootObject:artArray toFile:dataFilePath];
    }
}
-(BOOL)isMember:(int)postId
{
    for (MadhuriCategoryPost *tempCatObj in artArray)
    {
        if(tempCatObj.categoryPostId == postId)
            return YES;
    }
    return NO;
}
#pragma mark Table view methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 171;
}
// specify the height of your footer section
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    //differ between your sections or if you
    //have only on section return a static value
    if (showFooterView)
        return 71;
    else
        return 0;
}
// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [artArray count];
}
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    
    if(footerView == nil)
    {
        //allocate the view if it doesn't exist yet
        footerView  = [[UIView alloc] init];
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.frame = CGRectMake(5, 0.0, 763, 71);
		button.tag = 99;
        //  [button setTitle:@"Load more items ...." forState:UIControlStateNormal];
        button.titleLabel.font = [UIFont systemFontOfSize:30];
        button.titleLabel.textColor = [UIColor redColor];
        [button setBackgroundImage:[UIImage imageNamed:@"loadmore_off.png"] forState:UIControlStateNormal];
        [button setBackgroundImage:[UIImage imageNamed:@"loadmore_on.png"] forState:UIControlStateHighlighted];
        [button addTarget:self action:@selector(loadNextPage) forControlEvents:UIControlEventTouchUpInside];
        [footerView addSubview:button];
    }
    return footerView;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UILabel *lblTitle;
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        cell.accessoryType = UITableViewCellAccessoryNone;
        UIImageView *tempImg = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 171)];
        [tempImg setImage:[UIImage imageNamed:@"table_row_bg.png"]];
        cell.backgroundView = tempImg;
        
        UIImageView *tempselectedImg = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 171)];
        [tempselectedImg setImage:[UIImage imageNamed:@"table_row_bg_selected.png"]];
        cell.selectedBackgroundView = tempselectedImg;
        
        
        //Initialize Image View with tag 1.(Thumbnail Image)
        UIImageView *thumbImg = [[UIImageView alloc] initWithFrame:CGRectMake(30, 30, 120, 120)];
        thumbImg.tag = 1;
        thumbImg.contentMode = UIViewContentModeScaleToFill;
        thumbImg.layer.cornerRadius = 10;
        thumbImg.layer.masksToBounds = YES;
        thumbImg.layer.borderColor = [UIColor lightGrayColor].CGColor;
        thumbImg.layer.borderWidth = 1.0;
        [cell.contentView addSubview:thumbImg];
        
        //Initialize Label with tag 2.(Title Label)
        lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(thumbImg.frame.origin.x + thumbImg.frame.size.width + 40, cell.frame.origin.y + 65, self.view.frame.size.width - 260, cell.frame.size.height )];
        lblTitle.tag = 2;
        //lblTitle.shadowColor   = [UIColor blackColor];
        lblTitle.shadowOffset  = CGSizeMake(0.0, 2.0);
        lblTitle.font = TABLEVIEW_CELLLABEL_FONT;
        lblTitle.textAlignment = UITextAlignmentLeft;
        lblTitle.textColor = [UIColor blackColor];
        lblTitle.backgroundColor =  [UIColor clearColor];
        [self setShadow:lblTitle];
        [cell.contentView addSubview:lblTitle];
        
        
        //Initialize Image View with tag 3.(Arrow Image)
        /*  UIImageView *arrowImg = [[UIImageView alloc] initWithFrame:CGRectMake(290, 24, 14, 12)];
         [arrowImg setImage:[UIImage imageNamed:@"bigarrow.png"]];
         arrowImg.tag = 3;
         [cell.contentView addSubview:arrowImg];*/
        
    }
    if ([indexPath row] < [artArray count])
    {
        MadhuriCategoryPost *catObj = (MadhuriCategoryPost *)[artArray objectAtIndex:indexPath.row];
        UIImageView *thumbImgview = (UIImageView *)[cell viewWithTag:1];
        [thumbImgview setImageWithURL:[NSURL URLWithString:catObj.categoryPostThumbnail]
                     placeholderImage:[UIImage imageNamed:@"Icon.png"]
                              success:^(UIImage *image) {
                                  //DebugLog(@"success");
                              }
                              failure:^(NSError *error) {
                                  //DebugLog(@"write error %@", error);
                              }];
        
        lblTitle = (UILabel *)[cell viewWithTag:2];
        lblTitle.text = catObj.categoryPostTitle;
    }
    return cell;
    
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([indexPath row] < [artArray count])
    {
        MDWebViewController *typedetailController = [[MDWebViewController alloc] initWithNibName:@"MDWebViewController" bundle:nil] ;
        typedetailController.title = self.title;
        typedetailController.categoryId = indexPath.row;
        [self.navigationController pushViewController:typedetailController animated:YES];
    }
    [artTableView deselectRowAtIndexPath:[artTableView indexPathForSelectedRow] animated:NO];
}

-(void)setShadow:(UILabel *)label
{
    label.shadowColor = [UIColor whiteColor];
    label.shadowOffset = CGSizeMake(0.8, 0.0);
}

- (IBAction)alertActionMethod:(id)sender {
    
    UIButton *button=(UIButton *)sender;
    switch (button.tag) {
        case 0:
        {
            [self backAnimation];
            [conn cancel];
        }
            break;
            
        case 1:
        {
            [alertView setHidden:YES];
        }
            break;
        default:
            break;
    }
}

-(void)backAnimation
{
    //[self stopSectionAudio];
    [Flurry endTimedEvent:@"Art_Session" withParameters:nil];
    [UIView  transitionWithView:self.navigationController.view duration:1.0  options:UIViewAnimationOptionTransitionFlipFromRight
                     animations:^(void) {
                         BOOL oldState = [UIView areAnimationsEnabled];
                         [UIView setAnimationsEnabled:NO];
                         [self.navigationController popViewControllerAnimated:YES];
                         [UIView setAnimationsEnabled:oldState];
                     }
                     completion:nil];
}


- (void)viewDidUnload
{
    [self setArtTableView:nil];
    [self setLoaderImage:nil];
    [self setAlertView:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

////iOS 5
//- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
//{
//    return (interfaceOrientation == UIInterfaceOrientationPortrait);
//}
////iOS6
//- (BOOL)shouldAutorotate {
//    return NO;
//}
//- (NSUInteger)supportedInterfaceOrientations
//{
//    return UIInterfaceOrientationMaskPortrait;
//}
- (NSUInteger)supportedInterfaceOrientations{
#if __IPHONE_OS_VERSION_MAX_ALLOWED <= __IPHONE_5_1
    return UIInterfaceOrientationPortrait;
#else
    return UIInterfaceOrientationMaskPortrait;
#endif
}
- (IBAction)backActionMethod:(id)sender {
    if (goBack)
    {
        [self backAnimation];
    }
    else
    {
        [alertView setHidden:NO];
    }
}

#pragma mark PULL TO REFRESH
- (void)setupStrings{
    textPull = @"Pull down to refresh...";
    textRelease = @"Release to refresh...";
    textLoading = @"Loading please wait...";
}

- (void)addPullToRefreshHeader {
    refreshHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0 - REFRESH_HEADER_HEIGHT, 320, REFRESH_HEADER_HEIGHT)];
    refreshHeaderView.backgroundColor = [UIColor clearColor];
    
    refreshArrow = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"arrow.png"]];
    refreshArrow.frame = CGRectMake(floorf((REFRESH_HEADER_HEIGHT - 2) / 2),
                                    (floorf(REFRESH_HEADER_HEIGHT - 44) / 2),
                                    27*2, 75);
    
    refreshSpinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    refreshSpinner.frame = CGRectMake(floorf(floorf(REFRESH_HEADER_HEIGHT - 2) / 2), floorf((REFRESH_HEADER_HEIGHT - 20) / 2), 20, 20);
    refreshSpinner.hidesWhenStopped = YES;
    
    refreshLabel = [[UILabel alloc] initWithFrame:CGRectMake(refreshArrow.frame.origin.x+refreshArrow.frame.size.width+5, 0, 320, REFRESH_HEADER_HEIGHT)];
    refreshLabel.backgroundColor = [UIColor clearColor];
    refreshLabel.font = REFRESH_LABEL_FONT;
    refreshLabel.textAlignment = UITextAlignmentCenter;
    refreshLabel.textColor = [UIColor whiteColor];
    
    [refreshHeaderView addSubview:refreshArrow];
    [refreshHeaderView addSubview:refreshSpinner];
    [refreshHeaderView addSubview:refreshLabel];
    [artTableView addSubview:refreshHeaderView];
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    if (isLoading) return;
    isDragging = YES;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if (isLoading) {
        // Update the content inset, good for section headers
        if (scrollView.contentOffset.y > 0)
            artTableView.contentInset = UIEdgeInsetsZero;
        else if (scrollView.contentOffset.y >= -REFRESH_HEADER_HEIGHT)
            artTableView.contentInset = UIEdgeInsetsMake(-scrollView.contentOffset.y, 0, 0, 0);
    } else if (isDragging && scrollView.contentOffset.y < 0) {
        // Update the arrow direction and label
        [UIView animateWithDuration:0.25 animations:^{
            if (scrollView.contentOffset.y < -REFRESH_HEADER_HEIGHT) {
                // User is scrolling above the header
                refreshLabel.text = self.textRelease;
                [refreshArrow layer].transform = CATransform3DMakeRotation(M_PI, 0, 0, 1);
            } else {
                // User is scrolling somewhere within the header
                refreshLabel.text = self.textPull;
                [refreshArrow layer].transform = CATransform3DMakeRotation(M_PI * 2, 0, 0, 1);
            }
        }];
    }
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    if (isLoading) return;
    isDragging = NO;
    if (scrollView.contentOffset.y <= -REFRESH_HEADER_HEIGHT) {
        // Released above the header
        [self startLoading];
    }
}

- (void)startLoading {
    isLoading = YES;
    UIButton *loadBtn = (UIButton *)[footerView viewWithTag:99];
    loadBtn.enabled = FALSE;
    // Show the header
    [UIView animateWithDuration:0.3 animations:^{
        artTableView.contentInset = UIEdgeInsetsMake(REFRESH_HEADER_HEIGHT, 0, 0, 0);
        refreshLabel.text = self.textLoading;
        refreshArrow.hidden = YES;
        [refreshSpinner startAnimating];
    }];
    if([MDHDAppDelegate networkavailable])
    {
        if(artArray !=nil)
            [artArray removeAllObjects];
        [self pageRequest:1];
    }
    else
    {
        [self stopLoading];
    }
    // Refresh action!
    //[self refresh];
}

- (void)stopLoading {
    isLoading = NO;
    UIButton *loadBtn = (UIButton *)[footerView viewWithTag:99];
    loadBtn.enabled = YES;
    // Hide the header
    [UIView animateWithDuration:0.3 animations:^{
        artTableView.contentInset = UIEdgeInsetsZero;
        [refreshArrow layer].transform = CATransform3DMakeRotation(M_PI * 2, 0, 0, 1);
    }
                     completion:^(BOOL finished) {
                         [self performSelector:@selector(stopLoadingComplete)];
                     }];
}

- (void)stopLoadingComplete {
    // Reset the header
    refreshLabel.text = self.textPull;
    refreshArrow.hidden = NO;
    [refreshSpinner stopAnimating];
}

//- (void)refresh {
//    // This is just a demo. Override this method with your custom reload action.
//    // Don't forget to call stopLoading at the end.
//    [self performSelector:@selector(stopLoading) withObject:nil afterDelay:2.0];
//}
- (void)loadNextPage {
    DebugLog(@"Loadmore");
    isLoading = YES;
    refreshLabel.text = @"";
    UIButton *loadBtn = (UIButton *)[footerView viewWithTag:99];
    loadBtn.enabled = NO;
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    int nextPageNum = [defaults integerForKey:@"art"];
    DebugLog(@"nextPageNum %d",nextPageNum);
    [self pageRequest:nextPageNum+1];
}

@end
