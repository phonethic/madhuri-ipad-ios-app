//
//  MadhuriCategory.m
//  MadhuriDixit
//
//  Created by Rishi Saxena on 04/08/12.
//  Copyright (c) 2012 Phonethics. All rights reserved.
//

#import "MadhuriCategory.h"

@implementation MadhuriCategory

@synthesize categoryId=_categoryId;
@synthesize categorySlug=_categorySlug;
@synthesize categoryTitle=_categoryTitle;
@synthesize categoryDiscription=_categoryDiscription;
@synthesize categoryParent=_categoryParent;
@synthesize categoryPostCount=_categoryPostCount;

@end
