//
//  MDGalleryViewController.h
//  MadhuriDixitHD
//
//  Created by Rishi on 29/01/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MDAppDelegate.h"
#import "MWPhotoBrowser.h"
#import <QuartzCore/QuartzCore.h>

@interface MDGalleryViewController : UIViewController <MWPhotoBrowserDelegate>
{
    NSMutableData *responseData;
    NSError  *connectionError;
    NSURLConnection *conn;
    BOOL goBack;
    int status;
    NSMutableArray *galleryPhotos;
    NSArray *_photos;
}
@property (nonatomic, retain) NSArray *photos;
@property (strong, nonatomic) IBOutlet UIImageView *loaderImage;
@property (strong, nonatomic) IBOutlet UIView *alertView;
@property (nonatomic, copy) NSString *dataFilePath;

- (IBAction)backActionMethod:(id)sender;
- (IBAction)alertActionMethod:(id)sender;
@end
