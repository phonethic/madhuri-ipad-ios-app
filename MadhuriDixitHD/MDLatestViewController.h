//
//  MDLatestViewController.h
//  MadhuriDixitHD
//
//  Created by Rishi on 18/01/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MDLatestViewController : UIViewController
{
    NSMutableData *responseData;
    NSError  *connectionError;
    NSURLConnection *conn;
    BOOL goBack;
    
    int status;
    
    BOOL isDragging;
    BOOL isLoading;
    BOOL showFooterView;
}
@property (strong, nonatomic) IBOutlet UITableView *latestTableView;
@property (strong, nonatomic) IBOutlet UIView *alertView;
@property (strong, nonatomic) IBOutlet UIImageView *loaderImage;
@property (strong, nonatomic) NSMutableArray *latestArray;
@property (nonatomic, readwrite) NSInteger categoryPostCount;
@property (nonatomic, retain) UIView *refreshHeaderView;
@property (nonatomic, retain) UILabel *refreshLabel;
@property (nonatomic, retain) UIImageView *refreshArrow;
@property (nonatomic, retain) UIActivityIndicatorView *refreshSpinner;
@property (nonatomic, copy) NSString *textPull;
@property (nonatomic, copy) NSString *textRelease;
@property (nonatomic, copy) NSString *textLoading;
@property (nonatomic, copy) NSString *dataFilePath;
@property (nonatomic,strong)  UIView *footerView;

- (IBAction)backActionMethod:(id)sender;
- (IBAction)alertActionMethod:(id)sender;

- (void)setupStrings;
- (void)addPullToRefreshHeader;
- (void)startLoading;
- (void)stopLoading;
@end
