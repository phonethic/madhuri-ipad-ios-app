//
//  MDTitleViewController.m
//  MadhuriDixitHD
//
//  Created by Rishi on 17/01/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import "MDTitleViewController.h"
#import "MDAppDelegate.h"

@interface MDTitleViewController ()
- (void)postNotificationWithString:(NSString *)value;
@end

@implementation MDTitleViewController
@synthesize sliderButton;
@synthesize sliderbackImage;

- (id)initWithPageNumber:(int)page unpressedImageName:(NSString *)unpimage pressedImageName:(NSString *)pimage{
    if (self = [super initWithNibName:@"MDTitleViewController" bundle:nil])
    {
        pageNumber = page;
        unpressedimage=unpimage;
        pressedimage=pimage;
        DebugLog(@"----- page init = %d and scrolltype is %@ %@  \n",page ,pimage,unpimage);
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    if (![unpressedimage isEqualToString:@"slider1_transp.png"])
        [self.sliderbackImage setImage:[UIImage imageNamed:@"slider.png"]];
    else
        [sliderButton setUserInteractionEnabled:NO];
    
    [sliderButton setImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:unpressedimage ofType:@"png" ]] forState:UIControlStateNormal];
    [sliderButton setImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:pressedimage ofType:@"png" ]] forState:UIControlStateHighlighted];
    sliderButton.exclusiveTouch = TRUE;
    //[sliderButton addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchDown];
    
    //DebugLog(@"----- pressedimage %@ %d  \n",pressedimage ,sliderButton.userInteractionEnabled);
    
    UITapGestureRecognizer *singleTapGestureRecognizer = [[UITapGestureRecognizer alloc]
                                                          initWithTarget:self                                                                             action:@selector(singleTap:)];
    singleTapGestureRecognizer.numberOfTapsRequired = 1;
    singleTapGestureRecognizer.enabled = YES;
    singleTapGestureRecognizer.cancelsTouchesInView = NO;
    [self.sliderButton addGestureRecognizer:singleTapGestureRecognizer];
    
    [self.view addSubview:sliderButton];
    [self.view bringSubviewToFront:sliderButton];

}

-(void) singleTap:(id) sender
{
    [self.sliderbackImage setImage:[UIImage imageNamed:@"slider_glow.png"]];
    [self performSelector:@selector(changeImage) withObject:nil afterDelay:0.2];
}

- (void)changeImage
{
    [self.sliderbackImage setImage:[UIImage imageNamed:@"slider.png"]];
    [self postNotificationWithString:[NSString stringWithFormat:@"%d",pageNumber]];
}

- (void)postNotificationWithString:(NSString *)value //post notification method and logic
{
    NSDictionary *dictionary = [NSDictionary dictionaryWithObject:value forKey:titlenotificationKey];
    [[NSNotificationCenter defaultCenter] postNotificationName:titlenotificationName object:nil userInfo:dictionary];
}


- (void)viewDidUnload
{
    [self setSliderButton:nil];
    [self setSliderbackImage:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

////iOS 5
//- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
//{
//    return (interfaceOrientation == UIInterfaceOrientationPortrait);
//}
////iOS6
//- (BOOL)shouldAutorotate {
//    return NO;
//}
- (NSUInteger)supportedInterfaceOrientations{
#if __IPHONE_OS_VERSION_MAX_ALLOWED <= __IPHONE_5_1
    return UIInterfaceOrientationPortrait;
#else
    return UIInterfaceOrientationMaskPortrait;
#endif
}
@end
