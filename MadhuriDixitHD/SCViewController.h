//
//  SCViewController.h
//  MadhuriDixitHD
//
//  Created by Kirti Nikam on 22/01/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import <FacebookSDK/FacebookSDK.h>

@interface SCViewController : UIViewController  <FBUserSettingsDelegate>

@property (nonatomic, copy) NSString *FBtitle;
@property (nonatomic, copy) NSString *FBtLink;
@property (unsafe_unretained, nonatomic) IBOutlet UIImageView *appIcon;
@property (strong, nonatomic) NSMutableDictionary *postParams;
@property (strong, nonatomic) IBOutlet UITextView *message;
@property (strong, nonatomic) IBOutlet UIView *mainFBView;

- (IBAction)announce:(id)sender;
- (IBAction)loginFB:(id)sender;
- (IBAction)cancelModalView:(id)sender;

@end
