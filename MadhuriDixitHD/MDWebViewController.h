//
//  MDWebViewController.h
//  MadhuriDixitHD
//
//  Created by Sagar Mody on 20/01/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>
#define SYSTEM_VERSION_LESS_THAN(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)

@interface MDWebViewController : UIViewController<UIWebViewDelegate,UIActionSheetDelegate> {
    
}
@property (nonatomic, readwrite) NSInteger categoryId;
@property (nonatomic, copy) NSString *posttitle;
@property (nonatomic, copy) NSString *postLink;
@property (strong, nonatomic) IBOutlet UIWebView *contentview;
@property (strong, nonatomic) IBOutlet UIImageView *navBarImg;
@property (strong, nonatomic) IBOutlet UIImageView *loader_image;
@property (strong, nonatomic) IBOutlet UIButton *facebookshareBtn;
@property (strong, nonatomic) IBOutlet UIButton *backBtn;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *barbackBtn;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *barfwdBtn;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *barrefreshBtn;
@property (strong, nonatomic) IBOutlet UIToolbar *webviewtoolBar;

- (IBAction)backBtnPressed:(id)sender;
- (IBAction)webviewbackBtnPressed:(id)sender;
- (IBAction)webviewfwdBtnPressed:(id)sender;
- (IBAction)webviewrefreshBtnPressed:(id)sender;
- (IBAction)share_clicked:(id)sender;
@end
