//
//  MadhuriCategory.h
//  MadhuriDixit
//
//  Created by Rishi Saxena on 04/08/12.
//  Copyright (c) 2012 Phonethics. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MadhuriCategory : NSObject 

@property (nonatomic, readwrite) NSInteger categoryId;
@property (nonatomic, copy) NSString *categorySlug;
@property (nonatomic, copy) NSString *categoryTitle;
@property (nonatomic, copy) NSString *categoryDiscription;
@property (nonatomic, readwrite) NSInteger categoryParent;
@property (nonatomic, readwrite) NSInteger categoryPostCount;


@end
