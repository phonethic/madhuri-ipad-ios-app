//
//  MDTitleViewController.h
//  MadhuriDixitHD
//
//  Created by Rishi on 17/01/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MDTitleViewController : UIViewController {
    int pageNumber;
    NSString *pressedimage;
    NSString *unpressedimage;
}
@property (strong, nonatomic) IBOutlet UIButton *sliderButton;
@property (strong, nonatomic) IBOutlet UIImageView *sliderbackImage;

- (id)initWithPageNumber:(int)page unpressedImageName:(NSString *)pimage pressedImageName:(NSString *)unpimage;

@end
