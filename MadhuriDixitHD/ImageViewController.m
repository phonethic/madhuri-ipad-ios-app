//
//  ImageViewController.m
//  MadhuriDixitHD
//
//  Created by Rishi on 17/01/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import "ImageViewController.h"

@interface ImageViewController ()

@end

@implementation ImageViewController
@synthesize imgView;

- (id)initWithPageNumber:(int)page imageName:(NSString *)image height:(int)scrollheight {
    if (self = [super initWithNibName:@"ImageViewController" bundle:nil])
    {
        pageNumber = page;
        imageNameString=image;
        imageHeight=scrollheight;
        //   DebugLog(@"----- page init = %d and scrolltype is %@ %d \n",page ,imageNameString,imageHeight);
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    imgView.image = [UIImage imageNamed:imageNameString];
    //[imgView setAccessibilityIdentifier:imageNameString] ;
    [imgView setTag:pageNumber];
    [imgView setContentMode:UIViewContentModeScaleToFill];
    [self.view addSubview:imgView];
}

- (void)viewDidUnload
{
    [self setImgView:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

////iOS 5
//- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
//{
//    return (interfaceOrientation == UIInterfaceOrientationPortrait);
//}
////iOS6
//- (BOOL)shouldAutorotate {
//    return NO;
//}
- (NSUInteger)supportedInterfaceOrientations{
#if __IPHONE_OS_VERSION_MAX_ALLOWED <= __IPHONE_5_1
    return UIInterfaceOrientationPortrait;
#else
    return UIInterfaceOrientationMaskPortrait;
#endif
}
@end
