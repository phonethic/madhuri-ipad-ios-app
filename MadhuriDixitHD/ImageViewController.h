//
//  ImageViewController.h
//  MadhuriDixitHD
//
//  Created by Rishi on 17/01/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ImageViewController : UIViewController {
    int pageNumber;
    int imageHeight;
    NSString *imageNameString;
}
@property (strong, nonatomic) IBOutlet UIImageView *imgView;
- (id)initWithPageNumber:(int)page imageName:(NSString *)image height:(int)scrollheight;
@end
