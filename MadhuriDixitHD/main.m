//
//  main.m
//  MadhuriDixitHD
//
//  Created by Rishi on 15/01/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "MDAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([MDAppDelegate class]));
    }
}
