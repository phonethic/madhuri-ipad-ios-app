//
//  MDHealthBeautyViewController.h
//  MadhuriDixitHD
//
//  Created by Rishi on 18/01/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MDHealthBeautyViewController : UIViewController
{
    NSMutableData *responseData;
    NSError  *connectionError;
    NSURLConnection *conn;
    BOOL goBack;
    
    int status;
    BOOL isDragging;
    BOOL isLoading;
    BOOL showFooterView;
}

@property (strong, nonatomic) IBOutlet UITableView *healthbeautyTableView;
@property (strong, nonatomic) IBOutlet UIImageView *loaderImage;
@property (strong, nonatomic) IBOutlet UIView *alertView;
@property (strong, nonatomic) NSMutableArray *healthbeautyArray;
@property (nonatomic, retain) UIView *refreshHeaderView;
@property (nonatomic, retain) UILabel *refreshLabel;
@property (nonatomic, retain) UIImageView *refreshArrow;
@property (nonatomic, retain) UIActivityIndicatorView *refreshSpinner;
@property (nonatomic, copy) NSString *textPull;
@property (nonatomic, copy) NSString *textRelease;
@property (nonatomic, copy) NSString *textLoading;
@property (nonatomic, copy) NSString *dataFilePath;
@property (nonatomic,strong)  UIView *footerView;

- (void)setupStrings;
- (void)addPullToRefreshHeader;
- (void)startLoading;
- (void)stopLoading;

- (IBAction)backActionMethod:(id)sender;
@end
