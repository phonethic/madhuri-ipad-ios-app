//
//  SCViewController.m
//  MadhuriDixitHD
//
//  Created by Kirti Nikam on 22/01/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import "SCViewController.h"
#import "MDAppDelegate.h"


NSString *const kPlaceholderPostMessage = @"Say something about this...";

@interface SCViewController ()<UINavigationControllerDelegate>

@property (strong, nonatomic) FBUserSettingsViewController *settingsViewController;
@property (strong, nonatomic) IBOutlet FBProfilePictureView *userProfileImage;
@property (strong, nonatomic) UIActivityIndicatorView *activityIndicator;
@property (strong, nonatomic) IBOutlet UIButton *cancelButton;
@property (strong, nonatomic) IBOutlet UIButton *announceButton;
@property (strong, nonatomic) IBOutlet UIButton *loginFBBtn;
@property (strong, nonatomic) IBOutlet UILabel *userNameLabel;
- (void)populateUserDetails;
- (void)centerAndShowActivityIndicator;
@end

@implementation SCViewController
@synthesize userNameLabel = _userNameLabel;
@synthesize userProfileImage = _userProfileImage;
@synthesize announceButton = _announceButton;
@synthesize activityIndicator = _activityIndicator;
@synthesize cancelButton = _cancelButton;
@synthesize settingsViewController = _settingsViewController;
@synthesize postParams = _postParams;
@synthesize appIcon = _appIcon;
@synthesize message = _message;
@synthesize loginFBBtn = _loginFBBtn;
@synthesize FBtitle = _FBtitle;
@synthesize FBtLink = _FBtLink;


//- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
//{
//    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
//    if (self) {
//        // Custom initialization
//    }
//    return self;
//}

- (void)resetPostMessage
{
    self.message.text = @"";
    self.message.textColor = [UIColor blackColor];
}

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    // Clear the message text when the user starts editing
    //    if ([textView.text isEqualToString:kPlaceholderPostMessage]) {
    //        textView.text = @"";
    //        textView.textColor = [UIColor blackColor];
    //    }
    
    textView.textColor = [UIColor blackColor];
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    // Reset to placeholder text if the user is done
    // editing and no message has been entered.
    if ([textView.text isEqualToString:@""]) {
        [self resetPostMessage];
    }
}
#pragma mark open graph


// FBSample logic
// Handles the user clicking the Announce button, by either creating an Open Graph Action
// or first uploading a photo and then creating the action.
- (IBAction)announce:(id)sender {
    
    // Add user message parameter if user filled it in
    if (![self.message.text isEqualToString:kPlaceholderPostMessage] &&
        ![self.message.text isEqualToString:@""]) {
        [self.postParams setObject:self.message.text forKey:@"message"];
        [self.postParams setObject:self.FBtitle forKey:@"name"];
        [self.postParams setObject:self.FBtLink forKey:@"link"];
    }
    
    [FBRequestConnection
     startWithGraphPath:@"me/feed"
     parameters:self.postParams
     HTTPMethod:@"POST"
     completionHandler:^(FBRequestConnection *connection,
                         id result,
                         NSError *error) {
         NSString *alertText;
         if (error) {
             alertText = [NSString stringWithFormat:
                          @"error: domain = %@, code = %d",
                          error.domain, error.code];
         } else {
             //             alertText = [NSString stringWithFormat:
             //                          @"Posted action, id: %@",
             //                          [result objectForKey:@"id"]];
             alertText = @"Your message has been successfully posted on your facebook wall.";
         }
         // Show the result in an alert
         [[[UIAlertView alloc] initWithTitle:@"Madhuri Dixit"
                                     message:alertText
                                    delegate:nil
                           cancelButtonTitle:@"OK!"
                           otherButtonTitles:nil]
          show];
     }];
    
    [self dismissModalViewControllerAnimated:YES];
    
}


//- (void) alertView:(UIAlertView *)alertView
//didDismissWithButtonIndex:(NSInteger)buttonIndex
//{
//    [self resetPostMessage];
//
//}

- (void)centerAndShowActivityIndicator {
    CGRect frame = self.view.frame;
    CGPoint center = CGPointMake(CGRectGetMidX(frame), CGRectGetMidY(frame));
    self.activityIndicator.center = center;
    [self.activityIndicator startAnimating];
    
}
// FBSample logic
// Displays the user's name and profile picture so they are aware of the Facebook
// identity they are logged in as.
- (void)populateUserDetails {
    if (FBSession.activeSession.isOpen) {
        [[FBRequest requestForMe] startWithCompletionHandler:
         ^(FBRequestConnection *connection, NSDictionary<FBGraphUser> *user, NSError *error) {
             if (!error) {
                 //DebugLog(@"%@",user.name);
                 self.userNameLabel.text = user.name;
                 self.userProfileImage.profileID = [user objectForKey:@"id"];
                 _announceButton.enabled =  TRUE;
                 _announceButton.hidden = FALSE;
                 [_loginFBBtn setTitle:@"Logout" forState:UIControlStateNormal];
                 _loginFBBtn.hidden = TRUE;
             } else {
                 _announceButton.enabled =  FALSE;
                 _announceButton.hidden = TRUE;
                 _loginFBBtn.hidden = FALSE;
                 [_loginFBBtn setTitle:@"Log In" forState:UIControlStateNormal];
             }
         }];
    }
}

- (IBAction)loginFB:(id)sender {
    UIButton * button = (UIButton*) sender;
    if([button.titleLabel.text isEqualToString:@"Log In"]) {
        MDAppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
        [appDelegate openSessionWithAllowLoginUI:YES];
    } else {
        [FBSession.activeSession closeAndClearTokenInformation];
        [self resetPostMessage];
        _announceButton.enabled =  FALSE;
        [_loginFBBtn setTitle:@"Log In" forState:UIControlStateNormal];
    }
    
}

- (IBAction)cancelModalView:(id)sender {
    [self dismissModalViewControllerAnimated:YES];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.postParams =
    [[NSMutableDictionary alloc] initWithObjectsAndKeys:
     @"", @"link",
     @"http://madhuridixit-nene.com/uploads/Madhuri_App_Icon.png", @"picture",
     @"Title of Post", @"name",
     @"", @"caption",
     @"Check out this post.", @"description",
     nil];
    UIImage *cancelButtonImage;
    cancelButtonImage = [[UIImage imageNamed:@"DEFacebookSendButtonPortrait"] stretchableImageWithLeftCapWidth:4 topCapHeight:0];
    [self.cancelButton setBackgroundImage:cancelButtonImage forState:UIControlStateNormal];
    [self.announceButton setBackgroundImage:cancelButtonImage forState:UIControlStateNormal];
    [self.loginFBBtn setBackgroundImage:cancelButtonImage forState:UIControlStateNormal];
    //_cancelButton.frame = CGRectMake(8, 7, 63, 30);
    //    self.postParams =
    //    [[NSMutableDictionary alloc] initWithObjectsAndKeys:
    //     @"https://developers.facebook.com/ios", @"link",
    //     @"https://developers.facebook.com/attachment/iossdk_logo.png", @"picture",
    //     @"Facebook SDK for iOS", @"name",
    //     @"Build great social apps and get more installs.", @"caption",
    //     @"The Facebook SDK for iOS makes it easier and faster to develop Facebook integrated iOS apps.", @"description",
    //     nil];
    
    // Set up the post information, hard-coded for this sample
    //self.name.text = [self.postParams objectForKey:@"name"];
    //self.caption.text = [self.postParams objectForKey:@"caption"];
    //[self.caption sizeToFit];
    //self.description.text = [self.postParams objectForKey:@"description"];
    //[self.description sizeToFit];
    
    self.message.text = [NSString stringWithFormat:@"%@\n", self.FBtitle];
    
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]
                                              initWithTitle:@"Settings"
                                              style:UIBarButtonItemStyleBordered
                                              target:self
                                              action:@selector(settingsButtonWasPressed:)];
  
  
    self.activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    self.activityIndicator.hidesWhenStopped = YES;
    [self.view addSubview:self.activityIndicator];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(sessionStateChanged:)
                                                 name:SCSessionStateChangedNotification
                                               object:nil];
    self.mainFBView.layer.cornerRadius = 10;
    self.mainFBView.layer.masksToBounds = YES;
    //self.mainFBView.layer.borderColor = [UIColor whiteColor].CGColor;
    //self.mainFBView.layer.borderWidth = 2.0;
    //self.appIcon.layer.cornerRadius = 10;
    //self.appIcon.layer.masksToBounds = YES;
    //self.appIcon.layer.borderColor = [UIColor grayColor].CGColor;
    //self.appIcon.layer.borderWidth = 1.0;
    
    _announceButton.hidden = TRUE;
    _loginFBBtn.hidden = FALSE;
    
    //MDAppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
    [MDHDAppDelegate openSessionWithAllowLoginUI:NO];
    

}

/*
 * A simple way to dismiss the message text view:
 * whenever the user clicks outside the view.
 */
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *) event
{
    UITouch *touch = [[event allTouches] anyObject];
    if ([self.message isFirstResponder] &&
        (self.message != touch.view))
    {
        [self.message resignFirstResponder];
    }
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    if (FBSession.activeSession.isOpen) {
        [self populateUserDetails];
    }
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [_message becomeFirstResponder];
}

-(void)settingsButtonWasPressed:(id)sender {
    if (self.settingsViewController == nil) {
        self.settingsViewController = [[FBUserSettingsViewController alloc] init];
    }
    [self.navigationController pushViewController:self.settingsViewController animated:YES];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setCancelButton:nil];
    [self setAnnounceButton:nil];
    [self setUserNameLabel:nil];
    [self setMessage:nil];
    [self setLoginFBBtn:nil];
    [self setMainFBView:nil];
    [super viewDidUnload];
    [[NSNotificationCenter defaultCenter] removeObserver:self];

}

- (void)sessionStateChanged:(NSNotification*)notification {
    // A more complex app might check the state to see what the appropriate course of
    // action is, but our needs are simple, so just make sure our idea of the session is
    // up to date and repopulate the user's name and picture (which will fail if the session
    // has become invalid).
    [self populateUserDetails];
}

////iOS5
//- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
//    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
//}
//
////iOS6
//- (BOOL)shouldAutorotate {
//    return NO;
//}
- (NSUInteger)supportedInterfaceOrientations{
#if __IPHONE_OS_VERSION_MAX_ALLOWED <= __IPHONE_5_1
    return UIInterfaceOrientationPortrait;
#else
    return UIInterfaceOrientationMaskPortrait;
#endif
}
@end
