//
//  MDMoviesViewController.h
//  MadhuriDixitHD
//
//  Created by Rishi on 18/01/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>
@class MadhuriMovies;
@interface MDMoviesViewController : UIViewController <NSXMLParserDelegate>
{
    int status;
    MadhuriMovies *moviesObj;
    NSXMLParser *xmlParser;
    NSMutableData *responseAsyncData;
    BOOL goBack;
    
    BOOL isDragging;
    BOOL isLoading;
}

@property (strong, nonatomic) IBOutlet UITableView *moviesTableView;
@property (strong, nonatomic) IBOutlet UIImageView *loaderImage;
@property (strong, nonatomic) NSMutableArray *moviesArray;

@property (nonatomic, retain) UIView *refreshHeaderView;
@property (nonatomic, retain) UILabel *refreshLabel;
@property (nonatomic, retain) UIImageView *refreshArrow;
@property (nonatomic, retain) UIActivityIndicatorView *refreshSpinner;
@property (nonatomic, copy) NSString *textPull;
@property (nonatomic, copy) NSString *textRelease;
@property (nonatomic, copy) NSString *textLoading;

- (void)setupStrings;
- (void)addPullToRefreshHeader;
- (void)startLoading;
- (void)stopLoading;
- (IBAction)backActionMethod:(id)sender;
@end
