//
//  MDAppDelegate.m
//  MadhuriDixitHD
//
//  Created by Rishi on 15/01/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import "MDAppDelegate.h"
#import "MDViewController.h"
#import "MDMainViewController.h"
#import "Appirater.h"
#import <Parse/Parse.h>

#define FLURRY_APPID @"SKGPJRK62XYWPT7T6KGV"
#define APPIRATER_APPID @"599933783"

NSString *const SCSessionStateChangedNotification = @"com.facebook.MadhuriHD:MSessionStateChangedNotification";

@interface MDAppDelegate ()

@property (strong, nonatomic) SCViewController *mainViewController;

@end

@implementation MDAppDelegate
@synthesize networkavailable;
@synthesize navigationController = _navigationController;

- (NSUInteger)application:(UIApplication *)application supportedInterfaceOrientationsForWindow:(UIWindow *)window
{
    NSUInteger orientations = UIInterfaceOrientationMaskAll;
    
    if (self.window.rootViewController) {
        UIViewController* presented = [[(UINavigationController *)self.window.rootViewController viewControllers] lastObject];
        orientations = [presented supportedInterfaceOrientations];
    }
    return orientations;
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.
    self.viewController = [[MDMainViewController alloc] initWithNibName:@"MDMainViewController" bundle:nil];
    self.navigationController = [[UINavigationController alloc] initWithRootViewController:self.viewController];
    self.navigationController.navigationBar.tintColor=[UIColor blackColor];
    self.window.rootViewController = self.navigationController;
    [self.window makeKeyAndVisible];
    [Parse setApplicationId:@"rmlBqHyFyL8Qa2wCk57wPL6LQUHdgLUMgL43G4tT"
                  clientKey:@"k5t7Hb0Wb3YrlNl0u5AGhwmG6RiiXtfZVHXI7zXZ"];
    [application registerForRemoteNotificationTypes:UIRemoteNotificationTypeAlert|UIRemoteNotificationTypeBadge|UIRemoteNotificationTypeSound];
    [self startCheckNetwork];
    [self setAppRateAlert];
    [Flurry startSession:FLURRY_APPID];
    [Flurry logEvent:@"MAdhuri App Started"];
    [Appirater appLaunched:YES];
    return YES;
}

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    //DebugLog(@"didRegisterForRemoteNotificationsWithDeviceToken : devicetoken %@",deviceToken);
    [PFPush storeDeviceToken:deviceToken];
    //NSLog(@"----%@---",deviceToken);
    [PFPush subscribeToChannelInBackground:@"" block:^(BOOL succeeded, NSError *error) {
        if (succeeded)
            DebugLog(@"Successfully subscribed to broadcast channel!");
        else
            DebugLog(@"Failed to subscribe to broadcast channel; Error: %@",error);
    }];
}

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    DebugLog(@"didFailToRegisterForRemoteNotificationsWithError");
    if (error.code == 3010) {
        DebugLog(@"Push notifications are not supported in the iOS Simulator.");
    } else {
        // show some alert or otherwise handle the failure to register.
        DebugLog(@"application:didFailToRegisterForRemoteNotificationsWithError: %@", error);
	}
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    DebugLog(@"didReceiveRemoteNotification  : userInfo %@",userInfo);
    [PFPush handlePush:userInfo];
    
    UIApplicationState state = [application applicationState];
    if (state == UIApplicationStateInactive) {
        DebugLog(@"UIApplicationStateInactive");
    } else if (state == UIApplicationStateBackground){
        DebugLog(@"UIApplicationStateBackground");
    } else if (state == UIApplicationStateActive){
        DebugLog(@"UIApplicationStateActive");
    }
    
}


-(void) setAppRateAlert
{
    [Appirater setAppId:APPIRATER_APPID];
    [Appirater setDaysUntilPrompt:2];
    [Appirater setUsesUntilPrompt:2];
    [Appirater setSignificantEventsUntilPrompt:-1];
    [Appirater setTimeBeforeReminding:3];
    //[Appirater setDebug:YES];
}

-(void) startCheckNetwork
{
    
	[[NSNotificationCenter defaultCenter] addObserver: self selector: @selector(reachabilityChanged:) name: kReachabilityChangedNotification object: nil];
	
    internetReach = [Reachability reachabilityForInternetConnection];
	[internetReach startNotifier];
    [self updateReachabitlityFlag:internetReach];
    
}

-(void) stopCheckNetwork
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kReachabilityChangedNotification object:nil];
	internetReach = nil;
}

- (void)reachabilityChanged:(NSNotification *)note
{
	Reachability* curReach = [note object];
	NSParameterAssert([curReach isKindOfClass: [Reachability class]]);
	[self updateReachabilityStatus: curReach];
}

- (void) updateReachabilityStatus: (Reachability*) curReach
{
	if(curReach == internetReach)
	{
		[self updateReachabitlityFlag: curReach];
    }
}

- (void) updateReachabitlityFlag: (Reachability*) curReach
{
    NetworkStatus netStatus = [curReach currentReachabilityStatus];
    BOOL connectionRequired=[curReach connectionRequired];
    
    switch (netStatus)
    {
        case NotReachable:
        {
            networkavailable =  NO;
            break;
        }
            
        case ReachableViaWWAN:
        {
			if(connectionRequired==NO)
			{
                networkavailable = YES;
			}
			else
			{
				//this is for invoking internet library
				NSURLRequest *urlRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:@"maps.google.com"] cachePolicy:NO timeoutInterval:15.0] ;
				
				NSURLConnection *theConnection = [[NSURLConnection alloc] initWithRequest:urlRequest delegate:self];
				if (theConnection) {
					connectionRequired	= NO;
				}
			}
			break;
        }
        case ReachableViaWiFi:
        {
            if(connectionRequired==NO)
            {
                networkavailable = YES;
            }
            break;
		}
    }
    if(connectionRequired)
    {
		networkavailable = NO;
    }
    //DebugLog(@"network status = %d",networkavailable);
}

- (NSString *)filterHTMLCodes:(NSString *)String
{
    NSArray *filterArray = [NSArray arrayWithObjects: @"&#8211;", @"&#8230;", nil];
    for (int i=0; i<filterArray.count; i++) {
        NSRange range = [[String lowercaseString] rangeOfString:[[filterArray objectAtIndex:i] lowercaseString]];
        if(range.location != NSNotFound) {
            //Does contain the substring
            return [String stringByReplacingCharactersInRange:range withString:@""];
        }
    }
    return String;
}

- (void)saveImage: (UIImage*)image name:(int)lname
{
    if (image != nil)
    {
        NSFileManager *fileManager = [NSFileManager defaultManager];
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,  NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString *filename = [NSString stringWithFormat: @"%d.png",lname];
        //DebugLog(@"filename = %@" , filename);
        NSString* fullPath = [documentsDirectory stringByAppendingPathComponent:filename];
        //DebugLog(@"fullpath = %@" , fullPath);
        NSData* data = UIImagePNGRepresentation(image);
        //[data writeToFile:fullPath atomically:YES];
        BOOL success = [fileManager createFileAtPath:fullPath contents:data attributes:nil];
        DebugLog(@"%d",success);
    }
}

//Method writes a string to a text file
-(void) writeToTextFile:(NSString *)lcontent name:(NSString *)lname
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *fileName = [NSString stringWithFormat:@"%@.txt",lname];
    //DebugLog(@"filename = %@" , fileName);
    NSString* fullPath = [documentsDirectory stringByAppendingPathComponent:fileName];
    
    // DebugLog(@"fullpath = %@" , fullPath);
    
    NSError* error;
    if ([fileManager fileExistsAtPath:fullPath])
    {
        [self removeFile:fileName];
        [lcontent writeToFile:fullPath atomically:NO encoding:NSUTF8StringEncoding error:&error];
    } else {
        [lcontent writeToFile:fullPath atomically:NO encoding:NSUTF8StringEncoding error:&error];
    }
    //    if(error != nil)
    //        DebugLog(@"write error %@", error);
    //    else {
    //        DebugLog(@"file created %@", fileName);
    //    }
    
	
}

-(NSString *) getTextFromFile:(NSString *)lname
{
	NSFileManager *fileManager = [NSFileManager defaultManager];
    //get the documents directory:
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
	
	//make a file name to write the data to using the documents directory:
    NSString *fileName = [NSString stringWithFormat:@"%@.txt",lname];
    //DebugLog(@"filename = %@" , fileName);
    NSString* fullPath = [documentsDirectory stringByAppendingPathComponent:fileName];
    //DebugLog(@"fullpath = %@" , fullPath);
    NSError* error;
    if ([fileManager fileExistsAtPath:fullPath])
    {
        NSString *data = [NSString stringWithContentsOfFile:fullPath encoding:NSUTF8StringEncoding error:&error];
        if(error != nil)
            DebugLog(@"write error %@", error);
        return data;
    } else {
        return @"";
    }

}

- (void)removeFile:(NSString*)lname {
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *fullPath = [documentsDirectory stringByAppendingPathComponent:lname];
    BOOL success = [fileManager removeItemAtPath: fullPath error:NULL];
    //    if(success == YES)
    //        DebugLog(@" %@ removed",lname);
    //    else
    //        DebugLog(@" %@ NOT removed",lname);
}

- (void)saveNetworkStatus:(int)lval {
    
    // Store the data
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setInteger:lval forKey:@"status"];
    [defaults synchronize];
}

- (int)getNetworkStatus {
    
    // Store the data
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    return [defaults integerForKey:@"status"];
}

- (void)sessionStateChanged:(FBSession *)session
                      state:(FBSessionState)state
                      error:(NSError *)error
{
    // FBSample logic
    // Any time the session is closed, we want to display the login controller (the user
    // cannot use the application unless they are logged in to Facebook). When the session
    // is opened successfully, hide the login controller and show the main UI.
    switch (state) {
        case FBSessionStateOpen: {
            //DebugLog(@"success");
        }
            break;
        case FBSessionStateClosed:
        case FBSessionStateClosedLoginFailed:
            // FBSample logic
            // Once the user has logged in, we want them to be looking at the root view.
            //[self.navController popToRootViewControllerAnimated:NO];
            
            [FBSession.activeSession closeAndClearTokenInformation];
            
            //[self showLoginView];
            break;
        default:
            break;
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:SCSessionStateChangedNotification
                                                        object:session];
    
    if (error) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error"
                                                            message:error.localizedDescription
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
        [alertView show];
    }
}

- (BOOL)openSessionWithAllowLoginUI:(BOOL)allowLoginUI {
    NSArray *permissions = [NSArray arrayWithObjects:@"publish_actions", @"user_photos", nil];
    return [FBSession openActiveSessionWithPermissions:permissions
                                          allowLoginUI:allowLoginUI
                                     completionHandler:^(FBSession *session, FBSessionState state, NSError *error) {
                                         [self sessionStateChanged:session state:state error:error];
                                     }];
}

- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation {
    // FBSample logic
    // We need to handle URLs by passing them to FBSession in order for SSO authentication
    // to work.
    return [FBSession.activeSession handleOpenURL:url];
}


- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
     [Appirater appEnteredForeground:YES];
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
         [FBSession.activeSession close];
}

@end
