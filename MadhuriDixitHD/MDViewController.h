//
//  MDViewController.h
//  MadhuriDixitHD
//
//  Created by Rishi on 15/01/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MWPhotoBrowser.h"

@interface MDViewController : UIViewController <MWPhotoBrowserDelegate,NSXMLParserDelegate> {
    NSArray *_photos;
    NSMutableArray *galleryPhotos;
    int status;
    NSXMLParser *xmlParser;
    NSMutableData *responseAsyncData;
}
@property (nonatomic, retain) NSArray *photos;
- (IBAction)pushBtnPressed:(id)sender;
@end
