//
//  MDMainViewController.h
//  MadhuriDixitHD
//
//  Created by Rishi on 17/01/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MDAppDelegate.h"
#import "MWPhotoBrowser.h"
#import <QuartzCore/QuartzCore.h>

@interface MDMainViewController : UIViewController <UIScrollViewDelegate,UIGestureRecognizerDelegate,NSXMLParserDelegate> {
    NSMutableArray *viewControllers;
    BOOL pageControlUsed;
    NSArray *homeImages;
    
    NSArray *_photos;
    NSMutableArray *titleviewControllers;
    NSArray *titleImages;
    
    NSInteger lastOffset;
    NSInteger lastTitleOffset;
    
    int status;
    NSMutableArray *galleryPhotos;
    NSXMLParser *xmlParser;
    NSMutableData *responseAsyncData;
    int pageChanged;
    BOOL FLAG;
    int btn_pressed;
}
@property (nonatomic, retain) NSArray *photos;
@property (strong, nonatomic) IBOutlet UIImageView *menutextImageView;
@property (strong, nonatomic) IBOutlet UIImageView *taptoenterView;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet UIScrollView *titleScrollView;
@property (strong, nonatomic) IBOutlet UIImageView *slidingImageView;
@property (strong, nonatomic) IBOutlet UIImageView *backImageView;
@property (strong, nonatomic) IBOutlet UIPageControl *pageControl;
@property (strong, nonatomic) IBOutlet UIPageControl *pageControl1;
@property (nonatomic, retain) NSMutableArray *viewControllers;
@property (nonatomic, retain) NSMutableArray *titleviewControllers;
@property (strong, nonatomic) IBOutlet UIView *menuView;
@property (strong, nonatomic) IBOutlet UIButton *flipViewBtn;
@property (strong, nonatomic) IBOutlet UIButton *facebookBtn;
@property (strong, nonatomic) IBOutlet UIButton *twitterBtn;

- (IBAction)latestBtnPressed:(id)sender;
- (IBAction)danceBtnPressed:(id)sender;
- (IBAction)moviesBtnPressed:(id)sender;
- (IBAction)fashionBtnPressed:(id)sender;
- (IBAction)healthBtnPressed:(id)sender;
- (IBAction)artBtnPressed:(id)sender;
- (IBAction)flipViewBtnPressed:(id)sender;
- (IBAction)galleryBtnPressed:(id)sender;
- (IBAction)facebookTimeLineBtnPressed:(id)sender;
- (IBAction)twitterTimeLineBtnPressed:(id)sender;

- (IBAction)openWebsite:(id)sender;

- (void)loadScrollViewWithPage:(int)page type:(int) scrollviewType;
@end
